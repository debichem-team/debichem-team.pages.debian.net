---
layout: page
title: Prospective Packages in a Git Repository
description: >
  The site holds a list of prospective packages for the Debichem project, for
  which an initial packaging effort has already been placed in Git.
permalink: /prospectives/git/
---

These are prospective packages for the Debichem project, for which an initial
packaging effort has already been placed in Git. These packages might just need
some polishing to be ready to be uploaded into Debian. They might be a good
entry point in helping the Debichem project.

{% assign pkgs = site.PROSPECTIVE_PACKAGES | where_exp: "item","item.Vcs-Browser" | sort_natural: 'Name' -%}

{%- for pkg in pkgs -%}

## [{{ pkg.Name }}]({{ pkg.Homepage | default: pkg.Repository }}) {% if pkg.ITP or pkg.Vcs-Browser -%}
(
{%- if pkg.ITP -%}
[ITP](https://bugs.debian.org/{{ pkg.ITP }})
{%- if pkg.Vcs-Browser -%}
,
{%- endif -%}
{%- endif -%}
{%- if pkg.Vcs-Browser -%}
[VCS]({{ pkg.Vcs-Browser }})
{%- endif -%}
)
{%- endif %}

```text
* Package name    : {{ pkg.Package }}
  Version         : {{ pkg.Version }}
  Upstream Author : {{ pkg.Contact }}
* URL             : {{ pkg.Homepage | default: pkg.Repository }}
* License         : {{ pkg.License }}
  Programming Lang: {{ pkg.Lang }}
  Description     : {{ pkg.Description }}
```

{{ pkg.content | strip_html | markdownify }}

{% endfor %}

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
