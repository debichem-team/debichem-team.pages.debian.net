---
layout: page
title: Prospective Packages
description: >
  The site holds a list of prospective packages for the Debichem project. Just because a
  project is listed here does not mean it needs to or will be packaged
  eventually.
permalink: /prospectives/
---

These are prospective packages for the Debichem project. Just because a
project is listed here does not mean it needs to be packaged eventually, just
that it was deemed interesting (i.e. is open-source and chemistry-related) at
one point.

 * [All](/prospectives/all/ "List of all prospective packages")
 * [with ITP](/prospectives/itp/ "List of all prospective packages with an ITP")
 * [in GIT](/prospectives/git/ "List of all prospective packages with an initial packaging in Git")

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
