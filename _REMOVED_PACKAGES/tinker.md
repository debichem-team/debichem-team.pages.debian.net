---
Name: tinker
---

* [wnpp/tinker](https://salsa.debian.org/debichem-team/wnpp/tinker)

<!-- Revision: [3734](http://svn.debian.org/wsvn/debichem/?sc=1&rev=3734) -->

Reason: Outdated. Non-free. No maintainer.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
