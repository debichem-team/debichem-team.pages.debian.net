---
Name: chemical-structures
---

* [chemical-structures](https://salsa.debian.org/science-team/chemical-structures)

<!-- Revision: [3720](http://svn.debian.org/wsvn/debichem/?sc=1&rev=3720) -->

Reason: not maintained from within our tree

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
