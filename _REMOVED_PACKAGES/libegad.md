---
Name: libegad
---

* [wnpp/libegad](https://salsa.debian.org/debichem-team/wnpp/libegad)

<!-- Revision: [3733](http://svn.debian.org/wsvn/debichem/?sc=1&rev=3733) -->

Reason: ITP closed and archived, upstream site down, no rev-deps

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
