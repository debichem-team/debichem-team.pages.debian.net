---
layout: post
title: XCrySDen is back thanks to the FTP masters
author: Daniel Leidert
description: >
  The XCrySDen package is finally back in Debian after it had been removed due
  to an uncoordinated transition to TOGL 2.0.
date: '2019-12-26 12:43 +0100'
tag:
  - xcrysden
  - new
cagetory:
  - packages
---

The XCrySDen package is [finally back] in Debian. It was [removed] after Togl
had been updated to major release 2.0 without coordinating the transition.
Upstream then made some efforts to port the application and as of today,
XCrySDen hit the archive! Thanks to the FTP Masters.

[finally back]: https://packages.qa.debian.org/x/xcrysden/news/20191226T110018Z.html
[removed]: https://bugs.debian.org/932053

<!-- # vim: set tw=79 ts=2 sw=2 ai si et: -->
