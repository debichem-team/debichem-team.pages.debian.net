---
layout: post
title: Add pipeline status badges to all projects of a group
author: Daniel Leidert
date: '2019-12-22 12:30 +0100'
description: >
  Gitlab, used on salsa.debian.org, allows to add group badges. These badges
  will then appear for every project of a group. If your group has a policy
  to run jobs or pipelines these badges can show the status and they only
  have to be added at a central place once instead for every single project.
tag:
  - salsa.debian.org
  - salsa
  - groups
  - badges
category:
  - debian
  - salsa
---

The [debichem team on *salsa.debian.org*] uses continuous integration (CI)
[pipelines and jobs] to make sure the packages build and don't ship issues.
It is possible to add a [badge] to the projects overview page to show the status
of the last run pipeline/job as shown below:

{% include figure path="assets/images/debichem_shelxle_badge.png"
alt="Project page of ShelXle packaging showing the pipeline status badge"
caption="Project page of ShelXle packaging showing the pipeline status badge" %}

Because the team already has around 100 packages it would mean a lot of work to
add a badge to each packaging project. Fortunately [GitLab], the software used
for *salsa.debian.org*, provides a [way to add group badges] which will appear
on every projects overview page. The necessary setting can be done under the
group's settings page or the following URL

```
https://salsa.debian.org/groups/<GROUP>/-/edit
```

Expand the `Badges` section as shown below:

{% include figure path="assets/images/debichem_settings_badge.png"
alt="Expanded badge section of the group's settings page"
caption="Expanded badge section of the group's settings page" %}

To add the badge one has to provide a link and the corresponding badge URL. The
first one should point to project's (package's) pipeline page:

```
https://salsa.debian.org/%{project_path}/pipelines
```

and the badge URL is:

```
https://salsa.debian.org/%{project_path}/badges/%{default_branch}/pipeline.svg
```

Using the variables provided by Gitlab might be a good idea especially if a
group is using a mixture of branch names (e.g *master* vs. [*DEP14*]). Hitting the
`Add badge` button finally adds a badge to every project (package) under the
groups umbrella.

Below is a screenshot of these settings used in the debichem group:

{% include figure path="assets/images/debichem_pipeline_badge.png"
alt="Pipeline badge settings" caption="Pipeline badge settings" %}

[debichem team on *salsa.debian.org*]: https://salsa.debian.org/debichem-team "The debicheam team on salsa.debian.org"
[pipelines and jobs]: https://salsa.debian.org/salsa-ci-team/pipeline
[badge]: https://docs.gitlab.com/ee/user/project/badges.html
[GitLab]: https://about.gitlab.com/
[way to add group badges]: https://docs.gitlab.com/ee/user/project/badges.html#group-badges
[*DEP14*]: https://dep-team.pages.debian.net/deps/dep14/

*[CI]: Continuous Integration

<!-- # vim: set tw=79 ts=2 sw=2 ai si et: -->
