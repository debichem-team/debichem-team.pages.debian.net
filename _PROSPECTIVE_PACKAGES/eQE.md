---
Name: eQE (Embedded Quantum Espresso
Homepage: http://eqe.rutgers.edu/
Reference:
  - URL: http://aip.scitation.org/doi/10.1063/1.4897559
License: GPL2
Registration: http://eqe.rutgers.edu/download.html
---

Subsystem DFT (embedded) flavor of Quantum ESPRESSO.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
