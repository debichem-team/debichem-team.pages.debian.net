---
Name: Sparrow
Package: 
Version: 
Contact: 
Homepage: https://scine.ethz.ch/download/sparrow
Repository: https://github.com/qcscine/sparrow
License: BSD-3-Clause
Lang: 
Description: 
ITP: 
Vcs-Browser: 
Reference: 
  - URL: 
---

SCINE Sparrow is a command-line tool that implements many popular semiempirical
models. SCINE Sparrow contains methods which require a self-consistent solution
of a Roothaan-Hall-type equation (semiempirical methods based on the neglect of
diatomic differential overlap approximation and SCC-DFTB) and a
non-self-consistent methods (non-SCC DFTB). The application of semiempirical
models usually allows for rapid calculation of electronic energies and energy
gradients for a small molecular structure with a given charge and spin state.
The input is given in a user-friendly yaml format. Sparrow itself is able to
calculate energies, gradients with respect to nuclear coordinates, and Hessian
matrices (for vibrational frequencies and normal modes). More advanced
calculations such as structure optimizations and transition state searches can
be done through SCINE ReaDuct.

Current Features:

 * MNDO-type models: MNDO(/d), AM1, AM1*, RM1, PM3, PM6
 * DFTB models: non-SCC DFTB, SCC-DFTB, DFTB3

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
