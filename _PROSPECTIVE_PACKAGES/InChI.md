---
Name: InChI
Package: inchi
Version: 1.04
Contact: IUPAC and InChI Trust Limited
Homepage: https://www.inchi-trust.org
License: IUPAC-InChI Trust License 1.0 (more permissive then LGPL-2.1+)
Vcs-Browser: https://salsa.debian.org/debichem-team/inchi
Vcs-Git: https://salsa.debian.org/debichem-team/inchi.git
ITP: 814867
Lang: C
Description: International Chemical Identifier generator
---

Library to generate Standard/non-Standard InChI/InChIKey.

Many chemistry related softwares (at least OpenBABEL which has been already
packaged in debian) carry private copies of this library. This library should
be packaged for better compliance to debian policy which discourages inclusions
of 3rd party codes.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
