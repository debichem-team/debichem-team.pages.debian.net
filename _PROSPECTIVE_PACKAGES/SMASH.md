---
Name: SMASH
Homepage: http://smash-qc.sourceforge.net/
Repository: https://github.com/cmsi/smash
License: Apache 2.0
---

Scalable Molecular Analysis Solver for High-performance computing systems
(SMASH) is open-source software for massively parallel quantum chemistry
calculations written in the Fortran 90/95 language with MPI and OpenMP.
Hartree-Fock (HF), Second-order Moller-Plesset perturbation theory (MP2) and
Density Functional Theory (DFT) calculations can be performed.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
