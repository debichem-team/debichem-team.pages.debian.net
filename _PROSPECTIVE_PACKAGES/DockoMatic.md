---
Name: DockoMatic
Homepage: http://sourceforge.net/projects/dockomatic/
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.21864/abstract
  - URL: http://pubs.acs.org/doi/abs/10.1021/ci400047w
License: LGPL-3+
---

DockoMatic is a GUI application that is intended to ease and automate the
creation and management of AutoDock jobs for high throughput screening of
ligand/receptor interactions.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
