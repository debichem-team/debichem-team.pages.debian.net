---
Name: SHARC
Repository: https://github.com/sharc-md/sharc
Reference:
  - URL: https://onlinelibrary.wiley.com/doi/abs/10.1002/wcms.1370
License: GPLv3
---

The SHARC (Surface Hopping including ARbitrary Couplings) molecular dynamics
(MD) program suite is an ab initio MD software package developed to study the
excited-state dynamics of molecules.

SHARC
 * can treat non-adiabatic couplings at conical intersections, intersystem
   crossing induced by spin-orbit coupling, and laser couplings on an equal
   footing.
 * can treat any number of states of arbitrary multiplicities.
 * has interfaces to MOLPRO, MOLCAS, COLUMBUS, ADF, TURBOMOLE (only ricc2),
   GAUSSIAN, analytical potentials, and linear-vibronic coupling potentials.
 * comes with the WFoverlap program for efficient computation of nonadiabatic
   couplings and Dyson norms.
 * can do on-the-fly wave function analysis through TheoDORE.
 * includes auxiliary Python scripts for setup, maintenance and analysis of
   ensembles of trajectories.
 * includes scripts to perform excited-state (crossing point) optimizations and
   single point calculations with all interfaced methods.
 * has a comprehensive tutorial.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
