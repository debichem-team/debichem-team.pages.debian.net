---
Name: IboView
Homepage: http://www.iboview.org/index.html
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/anie.201410637/abstract
  - URL: http://pubs.acs.org/doi/abs/10.1021/ct400687b
License: GPL-3
---

IboView is a program for analyzing molecular electronic structure, based on
Intrinsic Atomic Orbitals (IAOs). IboView's main features include:

 * Visualization of electronic structure from first-principles DFT, in terms of
   intuitive concepts (partial charges, bond orders, bond orbitals)---even in
   systems with complex or unusual bonding.
 * Publication quality graphics, very fast visualizations, and a simple user
   interface
 * Import of wave functions from Molpro, Orca, Molcas, and Turbomole. IboView
   can also compute simple Kohn-Sham wave functions by itself, using the
   embedded MicroScf program. Additionally, IboView can be used as a plain
   orbital viewer if advanced analysis features are not required.
 * Visualization of electronic structure changes along reaction paths. Using
   the techniques described Electron flow in reaction mechanisms -- revealed
   from first principles and implemented in IboView, curly arrow reaction
   mechanisms can be determined directly from first principles!

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
