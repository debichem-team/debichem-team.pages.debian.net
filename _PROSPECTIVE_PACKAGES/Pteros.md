---
Name: Pteros
Homepage: http://pteros.sourceforge.net/
Lang: C++
Refernece:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.22989/abstract
License: Artistic 
---

Pteros is a C++ library for molecular modeling. It is designed to simplify the
development of custom programs for molecular modeling, analysis of molecular
dynamics trajectories and implementing new simulation algorithms. Pteros
provides facilities, which are routinely used in all such programs, namely
input/output in popular file formats, powerful and flexible atom selections,
geometry transformations, RMSD fitting and alignment, etc. Pteros also contains
powerful facilities for parsing command-line arguments in custom programs and
for running several analysis tasks in parallel, utilizing the power of modern
multi-core processors.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
