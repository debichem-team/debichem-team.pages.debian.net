---
Name: CCP1GUI
Homepage: http://www.stfc.ac.uk/CSE/randd/ccg/software/ccp1gui/25285.aspx
License: GPL
---

The GUI has a highly-featured interface for the GAMESS-UK program, and there
are working interfaces for Dalton, Molpro, ChemShell and MOPAC. It has arisen
as a result of demand within the UK academic community (principally through
CCP1) for a free, extensible Graphical User Interface (GUI) for community
codes, particularly for teaching purposes. The GUI has been built around the
Python open-source programming language and the VTK visualisation toolkit.

From its inception, the GUI was intended to work with a number of different
codes, so a variety of file formats are supported both for reading in
molecular structures and outputting data. As well as conventional formats
such as Z-matrix and PDB, there are programme-specific formats for CHARMM,
ChemShell, XMol and Gaussian, as well as support for XML. There is also an
interface to the OpenBabel chemistry toolbox via its Python bindings.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
