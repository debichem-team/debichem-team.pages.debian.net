---
Name: JADE
Homepage: http://jade-package.github.io/JADE
Repository: https://github.com/jade-package/JADE
Reference:
  - URL: http://pubs.acs.org/doi/abs/10.1021/ct501106d 
License: GLP-3
---

JADE is an on-the-fly surface hopping code for nonadiabatic molecular dynamics
of poly-atomic systems.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
