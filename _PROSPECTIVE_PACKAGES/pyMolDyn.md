---
Name: pyMolDyn
Homepage: https://pgi-jcns.fz-juelich.de/portal/pages/pymoldyn-main.html
Repository: https://github.com/sciapp/pyMolDyn
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.24697/abstract
License: MIT
---

pyMolDyn is a viewer for atomic clusters, crystalline and amorphous materials
in a unit cell corresponding to one of the seven 3D Bravais lattices. The
program calculates cavities (vacancies, voids) according to three definitions.

Features include:

 * Interactive viewer based on GR3
 * Computation of cavities in all seven 3D Bravais lattice systems
 * Creation of high resolution images appropriate for publications
 * Video creation from a set of input frames to analyze cavity changes in
   materials over time
 * Statistics including (i) Surfaces, volumes and surface/volume ratios of
   cavities and domains, (ii) Pair distribution functions (including cavities),
   bonds, bond (dihedral) angles and (iii) Gyration tensor, asphericity,
   acylindricity
 * Filter for atoms and cavities
 * Batch mode for simultaneous processing of multiple files

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
