---
Name: DL_MG
Homepage: https://ccpforge.cse.rl.ac.uk/gf/project/dl-mg/
Reference:
  - URL: https://pubs.acs.org/doi/10.1021/acs.jctc.7b01274
License: BSD 3-Clause
---

DL_MG is a parallel (MPI+OpenMP) 3D geometric high order finite difference
multigrid solver for Poisson and Poisson-Boltzmann Equations.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
