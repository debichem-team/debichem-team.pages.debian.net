---
Name: Pyrite
Homepage: https://durrantlab.pitt.edu/pyrite/
Repository: https://git.durrantlab.pitt.edu/jdurrant/pyrite
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.25155/abstract
License: GPL-3
---

Pyrite is a Blender plugin that imports the atomic motions captured by
molecular dynamics simulations. Using Pyrite, these motions can be rendered
using Blender's state-of-the-art computer-graphics algorithms. All 3D protein
representations (e.g., surface, ribbon, VDW spheres) are supported.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
