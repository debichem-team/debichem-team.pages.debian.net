---
Name: Environ
Homepage: http://www.quantum-environment.org
Reference:
  - URL: http://scitation.aip.org/content/aip/journal/jcp/136/6/10.1063/1.3676407
License: GPL
---

A quantum ESPRESSO module to handle environment effects in first-principles
quantum-mechanical simulations

Environ is a computational library aimed at introducing environment effects
into atomistic first-principles simulations, in particular for applications in
surface science and materials design.  A hierarchical, multiscale, strategy is
at the base of the different methods implemented: while the atomistic and
electronic details of the system of interest are fully preserved, the degrees
of freedom of the surrounding environment (being it a liquid solution of a more
complex embedding) are treated using simplified approaches. By reducing the
number of degrees of freedom and by exploiting intrinsic or faster statistical
averaging, the implemented methods allow the systematic un-expensive study of
large systems.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
