---
Name: MoViPAC
Homepage: http://www.reiher.ethz.ch/software/movipac
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.23036/abstract
License: GPL-3, LGPL-3
---

Vibrational Spectroscopy with a Robust Meta-Program for Massively Parallel
Standard and Inverse Calculations
 
Key features:
 * massively parallel calculation of full vibrational spectra (IR, Raman, ROA)
 * calculation of full vibrational spectra from results obtained with smaller
   model systems using a Cartesian transfer approach
 * inverse approaches to selectively calculate specific normal modes only (Mode-
   and Intensity-Tracking)
 * Analysis of normal modes in terms of localized vibrations

**NOTE:** This one seems to be a biggie, by highly respected researchers,
package

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
