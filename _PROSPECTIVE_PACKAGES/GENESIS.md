---
Name: GENESIS
Homepage: http://www.riken.jp/TMS2012/cbp/en/research/software/genesis/index.html
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/wcms.1220/abstract (OA, CC-BY-NC)
License: GPL-2
--- 

GENeralized-Ensemble Simulation System(GENESIS) is molecular dynamics and
modeling software for proteins or other biomolecular systems. It consists of
two simulators, SPDYN and ATDYN. SPDYN shows high performance on massively
parallel supercomputers like K computer in RIKEN AICS. ATDYN allows you to
perform multi-scale simulations using coarse-grained models and all-atom
models. Replica-exchange molecular dynamics methods or other
generalized-ensemble algorithms are currently available only in ATDYN.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
