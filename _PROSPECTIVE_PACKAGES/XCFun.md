---
Name: XCFun
Homepage: http://dftlibs.org/xcfun/
Reference:
  - URL: http://pubs.acs.org/doi/abs/10.1021/ct100117s
License: LGPL-3
---

XCFun is a library of DFT exchange-correlation (XC) functionals. It is based on
automatic differentiation and can therefore generate arbitrary order
derivatives of these functionals.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
