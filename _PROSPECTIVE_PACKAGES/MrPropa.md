---
Name: MrPropa
Homepage: http://ravel.pctc.uni-kiel.de/mrpropa/index.php/MrPropa
Reference:
  - URL: http://pubs.acs.org/doi/abs/10.1021/jp909362c
License: GPL-3+
---

MrPropa is a general program to solve the time-dependent and time-independent
Schrödinger equation for nearly arbitrary systems. The most important feature
is that there is no principal restriction concerning the number and kind of
DOFs which are treated explicitly and exactly. The exact treatment, however,
implies that the numerical effort scales exponentially with the number of DOFs.
Hence, in practical applications the number of DOFs which can be treated
exactly is limited to 4-6. For larger systems three reduced dimensionality
models of different accuracy can be applied. Another convenient feature of the
MrPropa program is, that the geometry of a molecule can be described by means
of a Z-matrix, as it is common in most quantum chemical programs.

**NOTE:** behind a user/password wall at
http://ravel.pctc.uni-kiel.de/mrpropa/index.php/Download_MrPropa

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
