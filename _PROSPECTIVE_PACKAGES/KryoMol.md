---
Name: KryoMol
Homepage: http://kde-apps.org/content/show.php/KryoMol?content=36260
License: GPL-2+
---

KryoMol is a KParts based program for analysis and visualization of chemical
data. Currently it can read many quantum chemical formats (Gaussian03/09,
NwChem, CFOUR, CPMD...) and 3D structures (mol,xyz...). Experimentally it can
display also 1D NMR and JCAMP-DX spectra.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
