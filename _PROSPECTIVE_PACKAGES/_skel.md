---
# The format follows https://wiki.debian.org/UpstreamMetadata with three
# exceptions:
#
# 1) License should follow the short license of d/copyright
# 2) Description follows the d/control format
# 3) ITP is the ITP bug number

Name: 
Package: 
Version: 
Contact: 
Homepage: 
Repository: 
License: 
Lang: 
Description: 
ITP: 
Vcs-Browser: 
Reference: 
  - URL: 
---

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
