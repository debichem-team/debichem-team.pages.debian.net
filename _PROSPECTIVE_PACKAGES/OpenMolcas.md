---
Name: OpenMolcas
Package: openmolcas
Repository: https://gitlab.com/Molcas/OpenMolcas
License: LGPL
Vcs-Browser: https://salsa.debian.org/debichem-team/openmolcas
Vcs-Git: https://salsa.debian.org/debichem-team/openmolcas.git
---

(Open)Molcas is a Quantum Chemistry software package developed by scientists to
be used by scientists.

The basic philosophy behind Molcas is to develop methods that allow accurate ab
initio treatment of very general electronic structure problems for molecular systems
in both ground and excited states which is not an easy task. Nowadays, knowledge
about how to obtain accurate properties for single-reference dominated ground states
is well developed, and Molcas contains a number of codes that can perform such
calculations (MP2, CC, CPF, DFT etc). All these methods treat the electron correlation
starting from a single determinant (closed or open shell) reference state. Such codes
are today’s standard in most Quantum Chemistry program.

However, Molcas is to be able to treat, highly degenerate states, such as those oc-
curring in excited states, transition states in chemical reactions, diradicaloid systems,
heavy metal systems, as well as other chemically important problems, all at the same
level of accuracy. This is a much more difficult problem, since a single-determinant
approach does not work well in these cases. The key feature of Molcas is the mul-
ticonfigurational approach. Molcas contains codes for general and effective multi-
configurational SCF calculations at the Complete Active Space (CASSCF) level, but
also employs more restricted MCSCF wave functions such as the Restricted Active
Space, RASSCF, and the Generalized Active Space, GASSCF. It is also possible using
either CASSCF or RASSCF to employ optimization techniques and obtain equilibrium
geometries, transition-state structures, force fields, and vibrational energies using gra-
dient techniques.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
