---
Name: RMG
Homepage: http://rmg.mit.edu/
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S0010465516300285
License: MIT
---

Reaction Mechanism Generator (RMG) is an automatic chemical reaction mechanism
generator that constructs kinetic models composed of elementary chemical
reaction steps using a general understanding of how molecules react.

CanTherm (a part of RMG) is a tool for computing the thermodynamic properties
of chemical species and high-pressure-limit rate coefficients for chemical
reactions using the results of a quantum chemistry calculation. Thermodynamic
properties are computed using the rigid rotor-harmonic oscillator approximation
with optional corrections for hindered internal rotors. Kinetic parameters are
computed using canonical transition state theory with optional tunneling
correction.

CanTherm can also estimate pressure-dependent phenomenological rate
coefficients k(T,P) for unimolecular reaction networks of arbitrary complexity.
The approach is to first generate a detailed model of the reaction network
using the one-dimensional master equation, then apply one of several available
model reduction methods of varying accuracy, speed, and robustness to simplify
the detailed model into a set of phenomenological rate coefficients. The result
is a set of k(T,P) functions suitable for use in chemical reaction mechanisms.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
