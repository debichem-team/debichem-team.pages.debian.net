---
Name: c2x
Homepage: http://www.check2xsf.org.uk/
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S0010465517304137
License: GPL-3
---

C2x provides a means of viewing isosurfaces from CASTEP, VASP and Onetep
.check, .castep_bin, .chdiff, .cell and .pdb files and can write .cell, .cml,
.pdb, .xyz, Gnuplot and .cube files. It can form supercells, generate k-point
meshes, find primitive cells, and generate k-points. It can extract band
energies and k-point positions from .check files. It can perform both Fourier
and trilinear interpolation to plot densities on given lines through cells. It
has some degree of support for writing .fdf, SHELX97 and .cif files, and
reading .res files.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
