---
Name: NECI
Repository: https://github.com/ghb24/NECI_STABLE
Reference:
  - URL: http://aip.scitation.org/doi/abs/10.1063/1.3193710
License: GPL-3
---

The Full Configuration Interaction Quantum Monte Carlo (FCIQMC) technique is a
stochastic method to compute the ground-state energy (and expectation values
over two-particle operators of the ground state) of extremely large many-body
Hamiltonians, usually in the context of ‘Full CI’ methods:  that is to say
electronic wavefunctions expanded in Slater determinant spaces comprising of
all possible determinants constructable from a given spatial orbital basis. 

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
