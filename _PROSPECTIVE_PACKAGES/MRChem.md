---
Name: MRChem
Homepage: http://mrchemdoc.readthedocs.io/en/latest/
Repository: https://github.com/MRChemSoft/mrchem
License: LGPL-3
---

MRChem is a numerical real-space code for molecular electronic structure
calculations within the self-consistent field (SCF) approximations of quantum
chemistry (Hartree-Fock and Density Functional Theory). The code is divided in
two main parts: the MultiResolution Computation Program Package (MRCPP), which
is a general purpose numerical mathematics library based on multiresolution
analysis and the multiwavelet basis which provide low-scaling algorithms as
well as rigorous error control in numerical computations, and the
MultiResolution Chemistry (MRChem) program that uses the functionalities of
MRCPP for computational chemistry applications.

Features as of July 2016:

 * Spin restricted (closed-shell) Kohn-Sham DFT (LDA and GGA)
 * Ground state energy and dipole moment
 * Localized orbitals
 * Shared memory parallelization (OpenMP)
 * Size limitation: ~50 orbitals
 * Accuracy limitation: ~nanoHartree

Upcoming features:

 * Hartree-Fock exchange
 * Spin unrestricted (open-shell) HF and DFT
 * Linear response properties (electric, magnetic)
 * Distributed memory parallelization (MPI + OpenMP)

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
