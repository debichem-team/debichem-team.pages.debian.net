---
Name: MESMER
Homepage: http://www.chem.leeds.ac.uk/mesmer.html
Repository: http://sourceforge.net/projects/mesmer/
Reference:
  - URL: http://pubs.acs.org/doi/abs/10.1021/jp3051033
License: LGPL-2
---

MESMER (Master Equation Solver for Multi Energy-well Reactions) models the
interaction between collisional energy transfer and chemical reaction for
dissociation, isomerisation, association, and non-adiabatic hopping processes.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
