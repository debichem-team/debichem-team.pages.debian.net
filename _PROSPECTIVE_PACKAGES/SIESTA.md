---
Name: SIESTA
Homepage: https://launchpad.net/siesta
Reference:
  - URL: http://iopscience.iop.org/article/10.1088/0953-8984/14/11/302/meta
License: GPL-3
---

SIESTA is both a method and its computer program implementation, to perform
efficient electronic structure calculations and ab initio molecular dynamics
simulations of molecules and solids. SIESTA's efficiency stems from the use of
strictly localized basis sets and from the implementation of linear-scaling
algorithms which can be applied to suitable systems. A very important feature
of the code is that its accuracy and cost can be tuned in a wide range, from
quick exploratory calculations to highly accurate simulations matching the
quality of other approaches, such as plane-wave and all-electron methods. The
possibility of treating large systems with some first-principles
electronic-structure methods has opened up new opportunities in many
disciplines. 

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
