---
Name: DL_POLY Classic
Package: dl-poly-classic
Version: 4.09.1
Released: 2018
Homepage: https://www.ccp5.ac.uk/DL_POLY_CLASSIC/
License: BSD
Lang: Fortran/Java
ITP: 797400
Description: general purpose molecular dynamics simulation package
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S0263785596000434
Vcs-Browser: https://salsa.debian.org/debichem-team/dl-poly-classic
Vcs-Git: https://salsa.debian.org/debichem-team/dl-poly-classic.git
---

DL_POLY Classic is a general purpose (parallel and serial) molecular dynamics
simulation package.

It can be executed as a serial or parallel application. The code achieves
parallelisation using the Replicated Data strategy, which is suitable for
homogeneous, distributed-memory, parallel computers. The code is useful for
simulations of up to 30,000 atoms with good parallel performance on up to 100
processors, though in some circumstances it can exceed or fail to reach these
limits.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
