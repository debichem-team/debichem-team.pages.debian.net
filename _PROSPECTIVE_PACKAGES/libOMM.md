---
Name: libOMM
Homepage: https://esl.cecam.org/LibOMM
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S0010465513004207
License: BSD
---

libOMM solves the Kohn-Sham equation as a generalized eigenvalue problem for a
fixed Hamiltonian. It implements the orbital minimization method (OMM), which
works within a density matrix formalism. The basic strategy of the OMM is to
find the set of Wannier functions (WFs) describing the occupied subspace by
direct unconstrained minimization of an appropriately-constructed functional.
The density matrix can then be calculated from the WFs. The solver is usually
employed within an outer self-consistency (SCF) cycle. Therefore, the WFs
resulting from one SCF iteration can be saved and then re-used as the initial
guess for the next iteration.

Features:

 * OMM functional of Mauri, Galli, and Car, and Ordejón et al.
 * Real (Γ-point) and complex (arb. k point) Hamiltonians
 * Efficient reuse of information within SCF loop and for multiple spins/k points
 * Simple (orthogonal basis) and generalized (non-orthogonal basis) eigenvalue problems
 * Cholesky factorization of generalized problem
 * Preconditioner for localized orbital basis

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
