---
Name: CCP4-Suite
Package: libccp4
Homepage: http://www.ccp4.ac.uk/html
Vcs-Browser: https://salsa.debian.org/science-team/libccp4
Vcs-Git: https://salsa.debian.org/science-team/libccp4.git
ITP: 721529
---

Collaborative Computational Project, Number 4. 1994.
"The CCP4 Suite: Programs for Protein Crystallography". Acta Cryst. D50, 760-763 

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
