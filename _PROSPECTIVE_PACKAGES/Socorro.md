---
Name: Socorro
Homepage: http://dft.sandia.gov/Socorro/about.html
License: GPL
---

Socorro is a modular, object oriented code for performing self-consistent
electronic-structure calculations utilizing the Kohn-Sham formulation of
density-functional theory. Calculations are performed using a plane wave basis
and either norm-conserving pseudopotentials or projector augmented wave
functions. Several exchange-correlation functionals are available for use
including the local-density approximation (Perdew-Zunger or Perdew-Wang
parameterizations of the Ceperley-Alder QMC correlation results) and the
generalized-gradient approximation (PW91, PBE, and BLYP). Both Fourier-space
and real-space projectors have been implemented, and a variety of methods are
available for relaxing atom positions, optimizing cell parameters, and
performing molecular dynamics calculations.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
