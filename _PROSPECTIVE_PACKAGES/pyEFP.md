---
Name: pyEFP
Repository: https://github.com/ale-odinokov/pyEFP
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.25149/abstract
License: LGPL-3
---

pyEFP is a library and tools to represent large molecules as a union of small
effective fragments.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
