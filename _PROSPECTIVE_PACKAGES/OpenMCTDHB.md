---
Name: OpenMCTDHB
Homepage: http://www.pci.uni-heidelberg.de/tc/usr/mctdhb/OpenMCTDHB.html
Reference:
  - URL: http://arxiv.org/abs/cond-mat/0703237
License: GPL-3
---

OpenMCTDHB is an open-source package for the many-body dynamics of ultracold
bosons. It is an implementation of the MCTDHB algorithm to solve the many-body
Schrödinger equation for bosons.  

OpenMCTDHB can solve the many-boson Schrödinger equation for 1D, 2D and 3D
problems at variable degree of accuracy.  The current version supports up to
two orbitals and thereby allows to assess the validity of any Gross-Pitaevskii
computation on the many-body level. The code is portable, serial and can be
compiled using open-source software only. A number of scripts that come with
the code allow to analyze the physics of a computation without the need to get
deeply involved. The goal of OpenMCTDHB is to make many-boson dynamics
accessible to everyone.

**NOTE:** likely not generally useful and too physics-oriented (only up to 3D/2 orbitals)

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
