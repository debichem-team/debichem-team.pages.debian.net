---
Name: Atomsk
Homepage: http://atomsk.univ-lille1.fr/
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S0010465515002817
License: GPL
---

Atomsk is a command-line program, developped under GNU/GPL licence, that aims
at creating, manipulating, and converting atomic systems, for the purposes of
ab initio calculations, classical potential simulations, or visualization. It
supports the file formats used by many programs, including VASP, Quantum
Espresso, LAMMPS, DL_POLY, IMD, Atomeye, or XCrySDen. Additionnaly Atomsk can
also perform some simple transformations of atomic positions, like the creation
of supercells, rotation, deformation, inserting dislocations

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
