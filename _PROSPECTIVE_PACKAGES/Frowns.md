---
Name: Frowns
Hompage: http://frowns.sourceforge.net
License: BSDish
---

Frowns is a chemoinformatics toolkit geared toward rapid development of
chemistry related algorithms.  It is written in almost 100% Python with a small
portion written in C++.

Frowns is loosely based on the PyDaylight API that Andrew Dalke wrote to wrap
the daylight C API.  In some cases programs written using PyDaylight will also
work under Frowns with a few minor changes.  A good overview of PyDaylight is
available at here.

A good place to look at what Smarts and Smiles are is at the daylight web site
located at http://www.daylight.com/

Frowns Features:

 * Smiles parser
 * Smarts substructure searching
 * SD file parser with SD field manipulations
 * Depiction for SD files with coordinates
 * Molecule Fingerprint generation
 * Several forms of Ring Detection available
 * Simple aromaticity perception
 * Everything's a graph (i.e. can form canonical strings from incomplete pieces of a molecule)
 * Really bad depiction of arbitray molecules! (requires AT&T's GraphViz)

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
