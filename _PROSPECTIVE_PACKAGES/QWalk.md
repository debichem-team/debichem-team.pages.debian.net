---
Name: QWalk
Homepage: http://www.qwalk.org/
Repository: https://github.com/QWalk/mainline
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S0021999109000424
License: GPL-2+
Comment: Other open-source QMC codes are QMCPACK and QMcBeaver
---

QWalk is a program developed to perform high accuracy quantum Monte Carlo
calculations of electronic structure in molecules and solids. It is
specifically designed as a research vehicle for new algorithms and method
developments, as well as being able to scale up to large system sizes.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
