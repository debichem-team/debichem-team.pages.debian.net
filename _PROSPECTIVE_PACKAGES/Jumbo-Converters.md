---
Name: Jumbo-Converters
Repository: https://bitbucket.org/wwmm/jumbo-converters
---

A set of libraries ("converters") which provide conversion to and from CML.

It contains converters for ChemDraw, ChemDrawXML, CIF, MolCAS, NWChem,
Gaussian, GamessUS, Quantum ESPRESSO, Turbomole, GamessUK, DALTON, MOPAC,
Jaguar, DLPoly, Amber, MDL Molfile, SDF, XYZ, PubchemSDF, PubchemXML, Bruker,
JDX, OWL.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
