---
Name: OSRA
Package: osra
Version: 2.0.0
Contact: Igor Filippov <igorf@helix.nih.gov>
Homepage: https://sourceforge.net/projects/osra/
License: GPL-2+
Lang: C++, Java
Description: optical structure recognition application
Reference:
  - URL: https://cactus.nci.nih.gov/osra/
ITP: 682760
Vcs-Browser: https://salsa.debian.org/debichem-team/osra
Vcs-Git: https://salsa.debian.org/debichem-team/osra.git
---

OSRA is a utility designed to convert graphical representations of chemical
structures, as they appear in journal articles, patent documents, textbooks,
trade magazines etc., into SMILES or SD files - a computer recognizable
molecular structure format. OSRA can read a document in any of the over 90
graphical formats parseable by ImageMagick - including GIF, JPEG, PNG, TIFF,
PDF, PS etc., and generate the SMILES or SDF representation of the molecular
structure images encountered within that document.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
