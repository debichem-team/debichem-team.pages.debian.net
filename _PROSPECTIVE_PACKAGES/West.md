---
Name: West
Homepage: http://www.west-code.org
Repository: http://greatfire.uchicago.edu/west-public
Version: 3.0.0
Reference:
  - URL: http://arxiv.org/abs/1501.03141
License: GPL-2+
---


<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
