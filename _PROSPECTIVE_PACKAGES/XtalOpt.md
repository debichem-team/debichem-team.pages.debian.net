---
Name: XtalOpt
Package: xtalopt
Version: r12.0
Released: 2019
Contact: David Lonie
Homepage: https://xtalopt.github.io
Repository: https://github.com/xtalopt/XtalOpt
License: GPLv2, LGPLv2 and BSD
Lang: C, C++
Description: Evolutionary Crystal Structure Prediction
ITP: 641655
Reference:
  - URL: https://www.sciencedirect.com/science/article/pii/S0010465510003140
---

XtalOpt is a free and truly open source evolutionary algorithm designed to
predict crystal structures. It is implemented as an extension to the Avogadro
molecular editor. See the list of publications for some of the systems it has
been used to explore.

XtalOpt runs on a workstation and supports using GULP, VASP, pwSCF (Quantum
ESPRESSO), and CASTEP for geometry optimizations. The calculations can be
performed remotely on a cluster running PBS or SGE, or on the workstation if a
computing cluster is not available. There is no special set up needed
server-side when running on remote queues, just install XtalOpt on the
workstation and it's ready to go!

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
