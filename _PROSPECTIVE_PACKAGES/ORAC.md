---
Name: ORAC
Homepage: http://www.chim.unifi.it/orac/
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.21388/abstract
License: GPL-2
---

ORAC is a suite to simulate at the atomistic level complex biosystems. It includes 
multiple time steps integration, Smooth Particle Mesh Ewald method, constant
pressure and constant temperature simulations. 

The present release has been supplemented with the most advanced techniques for
enhanced sampling in atomistic systems including replica exchange with solute
tempering, metadynamics on multidimensional reaction coordinates subspaces, and
potential of mean force reconstruction along arbitrary reaction paths using non
equilibrium molecular dynamics techniques. All these computational technologies
have been implemented on parallel architectures using the standard MPI
communication protocol.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
