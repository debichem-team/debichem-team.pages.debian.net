---
Name: libtensor
Homepage: http://iopenshell.usc.edu/downloads/tensor/
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.23377/abstract
License: Boost License
---

Tensor algebra library for computational chemistry.

The library is a set of performance linear tensor algebra routines for large
tensors found in post-Hartree-Fock methods. The routines are able to
dynamically balance the use of physical memory (RAM) and disk and perform
computations on multiple CPUs or CPU cores in a shared-memory environment. 

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
