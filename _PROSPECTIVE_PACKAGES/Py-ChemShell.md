---
Name: Py-ChemShell
Homepage: https://www.chemshell.org/
License: LGPL-3
Reference:
  - URL: https://pubs.acs.org/doi/10.1021/acs.jctc.8b01036
---

ChemShell is a scriptable computational chemistry environment with an emphasis
on multiscale simulation of complex systems using combined quantum mechanical
and molecular mechanical (QM/MM) methods. Motivated by a scientific need to
efficiently and accurately model chemical reactions on surfaces and within
microporous solids on massively parallel computing systems, Py-ChemShell
provides a modern platform for advanced QM/MM embedding models.  Py-ChemShell
is capable of performing QM/MM calculations on systems of significantly
increased size, which we illustrate with benchmarks on zirconium dioxide
nanoparticles of over 160000 atoms.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->

