---
Name: Towhee
Homepage: http://towhee.sourceforge.net/
License: GPL-2
---

Towhee is a Monte Carlo molecular simulation code originally designed for the
prediction of fluid phase equilibria using atom-based force fields and the
Gibbs ensemble with particular attention paid to algorithms addressing molecule
conformation sampling. The code has subsequently been extended to several
ensembles, many different force fields, and solid (or at least porous) phases.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
