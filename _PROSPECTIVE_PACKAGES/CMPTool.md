---
Name: CMPTool
Version: 2.1.4
Homepage: https://cmsportal.caspur.it/index.php/CMPTool
Comment: Download link broken, package is at https://cmsportal.caspur.it/CMPTool/CMPTool-2_1_4.tar.gz
License: unclear, likely FLOSS
---

CMPTool performs classical molecular dynamics simulations using short range
force models. With CMPTool you can run simulations in several ensembles using
advanced continuous dynamics algorithms. CMPTool makes use of CMSapi.  It can
run in serial or parallel (MPI).  

Features in Current Version:
 * very fast simulations of huge systems (up to millions of atoms) both in
   serial and parallel (MPI)
 * several short rate force models:
   - Stillinger-Weber
   - Tersoff
   - EDIP
 * simulation of NVE, NVT (Nose-Hoover chains) ensemble, velocity-rescaling and
   damped dynamics (for geometry optimizations);
 * an easy to extend molecular dynamics library CMSapi in C or Fortran;
 * output trajectory and restart ﬁle written in machine independent format,
   using Hierarchy Data File format (HDF5);

**NOTE:** Roadmap says it is currently suspended, but there was a recent Arxiv
paper (http://arxiv.org/abs/1206.5382) using it for advanced AIMD

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
