---
Name: IQmol
Homepage: http://iqmol.org/
Repository: https://github.com/nutjunkie/IQmol
License: GPL-3+
---

IQmol is a free open-source molecular editor and visualization package. It
offers a range of features including reading a variety of file formats, a
molecular editor, surface generation (orbitals and densities) and animations
(vibrational modes and reaction pathways).

It can read a variety of chemical  file formats including xyz, cml, pdb, mol,
fchk, cube data and Q-Chem input/output.  It also includes a free-form
molecular builder that allows arbitrary molecular structures to be created.
These structures can be optimized using molecular mechanics force  elds and
symmetrized to ensure the structure has the correct point group symmetry.  A
library of molecules and functional groups also exists, and these can be used
to facilitate building more complicated molecules

IQmol is capable of displaying a variety of molecular properties including
atomic charges, dipole moments and normal modes.  Several surface types can be
displayed including molecular orbitals, (spin) densities and van der Waals
surfaces. These surfaces can be colored according to an arbitrary scalar field,
such as the electrostatic potential. Animations are also available for
vibrational frequencies and reaction and optimization pathway

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
