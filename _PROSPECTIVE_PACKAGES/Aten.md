---
Name: Aten
Homepage: http://www.projectaten.net
Reference:
  - URL: http://www.ncbi.nlm.nih.gov/pubmed/19554555
License: GPL-3
---

Aten is a tool to create, edit, and visualise coordinates. It was written
primarily to be of use to people performing simulations of condensed matter,
but it may very well be useful to others out there dealing with non-periodic
systems.

Amongst other things, Aten can:

 * Provide a GUI for editing atomic coordinates
 * Create models, even periodic ones, from scratch
 * Randomly add in molecules to periodic boxes, allowing disordered systems to be created
 * Handle several crystal formats, and can replicate and scale unit cells
 * Load and save model data in several model formats out of the box
 * Cater for any model format you wish with its Filters system
 * Render other primitive object types, as well as surface/gridded data
 * Load in vibration/frequency data and animate it
 * Be fully scripted / automated from the command line
 * Load in and apply forcefield terms to a model with its easy-to-read typing language
 * Energy minimise (periodic) models
 * Run on Linux, Mac, and Windows

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
