---
Name: ezSpectrum
Repository: https://github.com/iopenshell/ezSpectrum
License: GPL
---

ezSpectrum computes stick photoelectron/photodetachment spectra for polyatomic
molecules within the double–harmonic approximation.

Franck-Condon factors (FCFs), the overlaps between the initial and target
vibrational wavefunctions, can be calculated:

 *  in the parallel normal modes approximation as products of one-dimensional
    harmonic wave functions;
 *  including Duschinsky rotations of the normal modes as full-dimensional
    integrals in full dimensionality including Duschinsky rotations of the
    normal modes.

In both cases the overlap integrals are computed analytically.

The calculation requires equilibrium geometries, harmonic frequencies, and
normal mode vectors for each electronic state, which can be computed by ab
initio packages. The key feature of ezSpectrum is the program–independent xml
input. It can be prepared either manually following the format description in
the manual and the examples provided, or by running the python script that
processes Q-Chem, ACESII, Molpro, GAMESS, and Gaussian outputs. The script can
be easily modified to process outputs of other programs.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
