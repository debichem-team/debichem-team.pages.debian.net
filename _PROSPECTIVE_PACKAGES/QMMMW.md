---
Name: QMMMW
Homepage: http://qe-forge.org/gf/project/qmmmw/
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S0010465515001678
License: GPL-3
---

QMMM wrapper is based on the original idea and code MS2 of Riccardo Di Meo.
QMMMW, works as an interface between the Born-Oppenheimer DFT engine of Quantum
ESPRESSO and the classical Dynamics engine of LAMMPS. 

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
