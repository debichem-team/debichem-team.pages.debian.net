---
Name: POLYANA
Homepage: http://cag.dat.demokritos.gr/Software.mol.php
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S0010465515002970
  - URL: http://arxiv.org/abs/1508.05374
License: MIT
---

A tool for the calculation of molecular radial distribution functions based on
Molecular Dynamics trajectories (from DL_POLY)

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
