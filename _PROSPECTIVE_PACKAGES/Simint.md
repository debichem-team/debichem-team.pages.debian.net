---
Name: Simint
Homepage: https://www.bennyp.org/research/simint/
Reference:
  - URL: http://doi.org/10.1002/jcc.24483
License: BSD
---

Simint is a vectorized implementation of the Obara-Saika (OS) method of
calculating electron repulsion integrals. Speedup is gained by vectorizing the
primitive loop of the OS algorithm, with additional vectorization and
optimizations left to the compiler.

Currently, Simint is limited to calculating ERI up to AM=7. It is not necessary
to compile up to that high of angular momentum; a lower angular momentum can be
selected at compile time with the SIMINT_MAXAM cmake option.

Simint is capable of calculating ERI with the angular momentum in any order.
That is, it is possible to calculate (ds|ds) and (sd|ds) directly, without
permutation of the resulting integrals.

Primitive screening is available.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
