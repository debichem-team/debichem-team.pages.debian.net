---
Name: Zeobuilder
Homepage: http://molmod.ugent.be/code/wiki/Zeobuilder
License:
  - URL: http://pubs.acs.org/doi/abs/10.1021/ci8000748
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/chin.200843225/abstract
License: GPL-3
---

Zeobuilder is a user-friendly GUI toolkit for the construction of advanced
molecular models. Zeobuilder's main goal is to become the ultimate molecular
editor, through an open and community-oriented development process.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
