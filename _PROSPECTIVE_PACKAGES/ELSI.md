---
Name: ELSI
Homepage: https://wordpress.elsi-interchange.org/
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S0010465517302941
License: BSD
---

ELSI provides and enhances scalable, open-source software library solutions for
electronic structure calculations in materials science, condensed matter
physics, chemistry, molecular biochemistry, and many other fields. ELSI focuses
on methods that solve or circumvent the Kohn-Sham eigenvalue problem in
density-functional theory. The ELSI infrastructure should also be useful for
other challenging eigenvalue problems.  

One of the key design pillars of ELSI is portability and support for various
computing environments, from laptop-type computers all the way to the most
efficient massively parallel supercomputers and new architectures (GPU and
manycore processors).

The libraries supported and enhanced in ELSI are:

 * ELPA (the Eigenvalue SoLvers for Petaflop-Applications)
 * libOMM (the Orbital Minimization Method)
 * PEXSI (the Pole EXpansion and Selected Inversion method)

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
