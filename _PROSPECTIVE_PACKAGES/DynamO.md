---
Name: DynamO
Homepage: https://www.marcusbannerman.co.uk/index.php/dynamo.html
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.21915/abstract
License: GPL-3+
---

DYNAMics of discrete Objects (DynamO) is a general, event driven simulation
code capable of simulating millions of particles in short times on commodity
computers.

Simple Systems:

 * Smooth or rough hard spheres, square wells, soft cores and all the other
   simple EDMD potentials.
 * Millions of particles for billions of events: Using 500 bytes per particle
   and running at around 75k events a second, even an old laptop can run huge
   simulations with accurate results.
 * Non-Equilibrium Molecular Dynamics (NEMD): DynamO has Lees-Edwards boundary
   conditions for shearing systems, thermalised walls and Andersen or rescaling
   thermostats.
 * Compression dynamics: Need high density systems? DynamO can compress any of
   its systems using isotropic compaction until the pressure diverges!
 * Poly-dispersity: All interactions are generalised to fully poly-disperse
   particles.

Granular Systems/Complex Boundaries:

 * Event driven dynamics with gravity: Almost all interactions work in the
   presence of gravity, allowing event driven simulations of macroscopic
   systems.
 * Sleeping particles: In systems where particles come to a rest, the sleeping
   particles algorithm can remove the cost of simulating the particles
   completely.
 * Triangle or sphere meshes: Arbitrary complex boundaries can be implemented
   using triangle meshes imported from CAD drawings.
 * Stepped potentials: Arbitrary stepped potentials may be simulated to
   approximate any soft potential.

Polymeric Systems/Accelerated Dynamics:

 * Parallel tempering/Replica exchange: Run multiple simulations in parallel
   and use this Monte Carlo technique to enhance the sampling of phase space.
 * Histogram reweighting: Combine the results from replica exchange simulations
   and extrapolate to temperatures which were not simulated.
 * Multicanonical simulations: Used in conjunction with replica exchange
   techniques, multicanonical simulations greatly enhance the sampling of phase
   space, helping find the true native state of the polymer.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
