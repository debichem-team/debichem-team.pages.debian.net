---
Name: CONQUEST
Homepage: http://www.order-n.org/
Reference:
  - URL: http://pubs.acs.org/doi/abs/10.1021/ct500847y
License: GPL
---

CONQUEST is a linear scaling DFT code which can be run at a number of different
levels of accuracy (from non-self-consistent ab initio tight binding up to
plane-wave accuracy full DFT). It features two different basis sets (numerical
atomic orbitals and B-splines), compatibility with SIESTA pseudo-atomic
orbitals and the FHI pseudopotentials provided by ABINIT and the ability to
perform exact diagonalisation as well as linear scaling solution.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
