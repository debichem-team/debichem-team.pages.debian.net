---
Name: IMD
Homepage: http://imd.itap.physik.uni-stuttgart.de/
Reference:
  - URL: http://www.worldscientific.com/doi/abs/10.1142/S0129183197000990
License: GPL
---

IMD is a software package for classical molecular dynamics simulations. Several
types of interactions are supported, such as central pair potentials, EAM
potentials for metals, Stillinger-Weber and Tersoff potentials for covalent
systems, and Gay-Berne potentials for liquid crystals. A rich choice of
simulation options is available: different integrators for the simulation of
the various thermodynamic ensembles, options that allow to shear and deform the
sample during the simulation, and many more. There is no restriction on the
number of particle types.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
