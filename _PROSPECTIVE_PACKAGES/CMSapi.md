---
Name: CMSapi
Homepage: https://cmsportal.caspur.it/index.php/CMSapi
License: unclear, likely FLOSS
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S0010465505001992
  - URL: http://144.206.159.178/ft/216/572311/12812743.pdf
---

CMSapi is an Application Programming Interface (library of functions) for
classical and tight binding molecular dynamics. The aim of CMSapi is to provide
building blocks for quickly writing efficient codes for atomistic simulations.
CMSapi provide most of the component required for writing a modern molecular
dynamics program. Among the others, CMSapi provide sympletic integrator for
several ensembles (NVE, NVT, NPH, NPT), several schemes for computing
interaction lists, potentials (currently the list is restrictet to those of
interest for materials science), and functions for calculating standard
quantities such that kinetic energy, pressure and many more.

Features:

 * sympletic integrator
 * ensemble NVE, NVT (using Nose-Hoover chains), NPH and NPT (fully flexible
   cells)
 * schemes for complete and incomplete simple interaction and combined
   Verlet+Linked Cells lists
 * improved implementation scheme for MIC algorithm
 * reordering algorithm scheme
 * empirical potentials: Lenard-Jones, Stillinger-Weber, Tersoff, EDIP
 * semi-empirical tight-binding with multicomponents parametrization
 * routines to calculate properties (such as energetics, box geometry, fix
   momentum, etc.)
 * specific mathematical library kernels for atomistic simulation not included
   in standard BLAS and LAPACK

**NOTE:** appears to be only available as part of CMPTool

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
