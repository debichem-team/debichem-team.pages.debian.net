---
Name: gCP
Homepage: http://www.thch.uni-bonn.de/tc/downloads/gcp/index.html
Reference:
  - URL: http://aip.scitation.org/doi/10.1063/1.3700154
License: GPL-1+
---

A computationally inexpensive approach to correcting for BSSE

A major discouragement for wider use of counterpoise correction is its
computational cost. With the geometric counterpoise scheme (gCP), Kruse and
Grimme offer a semi-empirical approach that is extremely cost effective and
appears to strongly mimic the traditional counterpoise correction.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
