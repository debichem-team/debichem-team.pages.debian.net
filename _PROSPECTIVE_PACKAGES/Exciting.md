---
Name: Exciting
Package: exciting
Version: 13 (nitrogen)
Released: 2018
Contact: Claudia Ambrosch-Draxl <claudia.ambrosch-draxl@mu-leoben.at>
Homepage: http://exciting-code.org
License: GPL
Lang: Fortran 90
Description: All-electron full-potential electronic-structure code
ITP: 602097
Reference: 
  - URL: http://iopscience.iop.org/0953-8984/26/36/363202
---

exciting is a full-potential all-electron density-functional-theory (DFT)
package based on the linearized augmented plane-wave (LAPW) method. It can be
applied to all kinds of materials, irrespective of the atomic species involved,
and also allows for the investigation of the atomic-core region.

Its particular focus is on excited state properties, within the framework of
time-dependent DFT (TDDFT) as well as within many-body perturbation theory
(MBPT).

General features:

 * High precision all-electron DFT code based on the FP-LAPW method including
   local-orbitals
 * Various xc functionals available
 * Calculation of forces and structural optimization
 * Treatment of magnetism in the most general way, including spin-orbit
   coupling and non-collinear magnetism
 * Plotting of band structure, Fermi surface, charge density, potential etc.
   (1D, 2D and 3D)
 * Visualization with xmgrace and XCrySDen supported
 * MPI k-point parallelization, as well as optimization for multithreaded
   numeric libraries (BLAS LAPACK)

Excited states features:

 * Macroscopic dielectric function within time-dependent DFT and the
   Bethe-Salpeter equation
 * Available exchange-correlation kernels: RPA, ALDA, long-range contribution
   model-kernels, BSE-derived kernel
 * RPA and ALDA loss function for finite momentum transfer q-vectors

This package will be part of the Debian Science nanoscale-physics
metapackage.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
