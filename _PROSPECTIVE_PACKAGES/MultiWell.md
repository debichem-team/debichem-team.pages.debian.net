---
Name: MultiWell
Homepage: http://aoss-research.engin.umich.edu/multiwell/
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/kin.20447/abstract
License: GPL-2
---

Calculates time dependent concentrations, reaction yields, vibrational
distributions, and rate constants as functions of temperature and pressure for
multi-well, multi-channel unimolecular reactions systems that consist of stable
species (wells) and multiple isomerization and/or dissociation reactions
(channels). The stochastic method is used to solve the time-dependent Master
Equation.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
