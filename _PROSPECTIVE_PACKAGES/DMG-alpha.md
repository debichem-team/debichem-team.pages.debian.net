---
Name: DMG-alpha
Homepage: http://ww2.ii.uj.edu.pl/~szczelir/?category=dmga_test&lang=en
Reference:
  - URL: http://pubs.acs.org/doi/abs/10.1021/ci500273s
License: Modified BSD
---

DMG-Alpha is capable of computing Power Diagrams in 3D for box geometries with
arbitrary periodic information. All geometry constructs are easily accessible,
and all kinds of attributes can be simply computed, e.g. areas of cell sides,
volumes of cells, neighbouring information.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
