---
Name: RPMDrate
Homepage: http://ysuleyma.scripts.mit.edu/index.html
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S0010465512003608
License: MIT
---

RPMDrate is an implementation of the ring polymer molecular dynamics (RPMD)
method for calculating chemical reaction rates.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
