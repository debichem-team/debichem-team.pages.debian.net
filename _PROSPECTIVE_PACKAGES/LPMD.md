---
Name: LPMD
Homepage: http://www.lpmd.cl
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S001046551000336X
License: GPL
---

Las Palmeras Molecular Dynamics (LPMD) is a Molecular Dynamics (MD) code
written from scratch in C++, as user-friendly, modular and multiplatform as
possible. Some of its features are that it is an Open Source code, it works
using plugins, it reads simple and intuitive configuration files, and it also
includes utility software to perform analysis, conversion, and visualization of
MD simulations.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
