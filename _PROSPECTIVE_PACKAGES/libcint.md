---
Name: libcint
Homepage: http://wiki.sunqm.net/libcint
Repository: https://github.com/sunqm/libcint
Reference:
  - URL: http://arxiv.org/abs/1412.0649
License: BSD
---

libcint is an open source library for analytical Gaussian integrals.
It provides C/Fortran API to evaluate one-electron / two-electron
integrals for Cartesian / real-spheric / spinor Gaussian type functions.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
