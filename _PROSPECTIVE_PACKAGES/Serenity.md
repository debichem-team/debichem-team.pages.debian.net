---
Name: Serenity
Homepage: https://thclab.uni-muenster.de/serenity/serenity
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.25162/abstract
License: LGPLv3
Comment: Behind Gitlab instance.
---

Serenity implements a wide variety of functionalities with a focus on subsystem
methodology. The modular code structure in combination with publicly available
external tools and particular design concepts ensures extensibility and
robustness with a focus on the needs of a subsystem program. Several important
features of the program are exemplified with sample calculations with subsystem
density-functional theory, potential reconstruction techniques, a
projection-based embedding approach and combinations thereof with geometry
optimization, semi-numerical frequency calculations and linear-response
time-dependent density-functional theory. 

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
