---
Name: BigDFT
Homepage: http://bigdft.org/
Reference:
  - URL: http://aip.scitation.org/doi/abs/10.1063/1.2949547
License: GPL
---

BigDFT is a DFT massively parallel electronic structure code using a wavelet
basis set. Wavelets form a real space basis set distributed on an adaptive mesh
(two levels of resolution in our implementation). GTH or HGH pseudopotentials
are used to remove the core electrons. Thanks to our Poisson solver based on a
Green function formalism, periodic systems, surfaces and isolated systems can be
simulated with the proper boundary conditions. 

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
