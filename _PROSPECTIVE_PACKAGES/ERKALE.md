---
Name: ERKALE
Repository: https://github.com/susilehtola/erkale
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.22987/abstract
License: GPL-2
---

ERKALE is a quantum chemistry program used to solve the electronic structure of
atoms, molecules and molecular clusters. It is developed at the University of
Helsinki.
.
The main use of ERKALE is the computation of x-ray properties, such as
ground-state electron momentum densities and Compton profiles, and core (x-ray
absorption and x-ray Raman scattering) and valence electron excitation spectra
of atoms and molecules.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
