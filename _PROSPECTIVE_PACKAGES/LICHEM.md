---
Name: LICHEM
Repository: https://github.com/kratman/LICHEM_QMMM
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.24295/abstract
License: GPL-3
---

This package is designed to be an interface between QM and MM software so that
QMMM calculations can be performed with polarizable and frozen electron density
force fields. Functionality is also present for standard point-charge based
force fields, pure MM, and pure QM calculations.

 * Available calculations: single-point energies, geometry optimizations,
classical Monte Carlo, path-integral Monte Carlo, and reaction pathways.

 * Available QM wrappers: Gaussian, NWChem, PSI4

 * Available MM wrappers: TINKER

 * MM wrappers in development: AMBER, LAMMPS, OpenMM

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
