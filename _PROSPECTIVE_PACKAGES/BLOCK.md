---
Name: BLOCK
Homepage: http://chemists.princeton.edu/chan/software/block-code-for-dmrg/ 
Reference:
  - URL: http://scitation.aip.org/content/aip/journal/jcp/116/11/10.1063/1.1449459
  - URL: http://scitation.aip.org/content/aip/journal/jcp/136/12/10.1063/1.3695642
License: GPL
---

DMRG is a variational wavefunction method. Relative to other quantum chemical
methods,  its strength lies in its ability to efficiently describe strong or
multi-reference correlation. It is especially suited to correlation that has a
one-dimensional topology, i.e. where the orbitals are arranged with a chain-,
strip-, or ring-like connectivity. If the correlation is dynamic in character,
the DMRG may still be used, but other methods such as coupled cluster theory
will usually be more computationally efficient. The recommended use of the DMRG
is to treat correlation in active spaces too large for complete active space
(CAS) techniques. Thus, if you are interested in (i) a CAS-like treatment of
low-lying eigenstates in problems with 16-40 active orbitals, or (ii)
one-dimensional orbital topologies with up to 100 active orbitals, and (iii)
standard chemical accuracy (1 kcal/mol in energy differences), then DMRG
may be the right method for you.

Features:

 * DMRG sweep algorithm for quantum chemistry, Hubbard and Heisenberg
   hamiltonians
 * Full spin-adaptation (SU(2) symmetry) and Abelian point-group symmetries
 * State-averaged and state-specific excited states
 * one-, two-, three- and four-particle density matrices
 * one- and two-particle transition density matrices between two states
 * perturbation methods including NEVPT2 and MPSPT
 * DMRG-SCF or/and DMRG-NEVPT2 interfaces to the Molpro, ORCA, Q-Chem and
   Molcas program packages

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
