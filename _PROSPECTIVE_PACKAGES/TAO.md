---
Name: TAO
Homepage: http://www.mcs.anl.gov/research/projects/tao/index.html
License: BSDish
---

The TAO project focuses on the development of software for large-scale
optimization problems. TAO uses an object-oriented design to create a flexible
toolkit with strong emphasis on the reuse of external tools where appropriate.
Our design enables bi-directional connection to lower level linear algebra
support (for example, parallel sparse matrix data structures) as well as higher
level application frameworks. 

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
