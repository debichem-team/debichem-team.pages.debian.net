---
Name: ls1 mardyn
Homepage: http://www.ls1-mardyn.de/
Repository: https://github.com/andersx/ls1-mardyn
Reference:
  - URL: http://pubs.acs.org/doi/abs/10.1021/ct500169q
License: BSD-2-clause
---

The molecular dynamics (MD) simulation program ls1 mardyn was optimized for
massively parallel execution on supercomputing architectures. The acronym
stands for large systems 1: molecular dynamics. With an efficient MD simulation
engine, explicit particle-based force-field models of the intermolecular
interactions can be applied to length and time scales which were previously out
of scope for molecular methods. Employing a dynamic load balancing scheme for
an adaptable volume decomposition, ls1 mardyn delivers a high performance even
for challenging heterogeneous configurations.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
