---
# vim: set tw=79 ts=2 sw=2 ai si et:
Name: MOIL-opt
Homepage: https://omictools.com/moil-tool
License: public-domain
Description: n/a
Reference:
  - URL: https://pubs.acs.org/doi/abs/10.1021/ct200360f
---

n/a
