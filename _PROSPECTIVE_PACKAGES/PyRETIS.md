---
Name: PyRETIS
Homepage: http://www.pyretis.org/
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.24900/abstract
License: LGPL-2.1+
---

PyRETIS is a Python library for rare event molecular simulations with emphasis
on methods based on transition interface sampling and replica exchange
transition interface sampling. The PyRETIS library can be used to set up
tailored simulations or one can use a Python flavored input file to run
different kinds of path sampling simulations.

PyRETIS is open source, easy to use and can be interfaced with other simulation
packages such as GROMACS or CP2K.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
