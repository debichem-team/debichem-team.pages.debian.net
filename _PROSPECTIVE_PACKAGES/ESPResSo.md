---
Name: ESPResSo
Homepage: http://espressomd.org
Repository: https://github.com/espressomd/espresso
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S001046550500576X
License: GPL-3
---

ESPResSo is a highly versatile software package for performing and analyzing
scientific Molecular Dynamics many-particle simulations of “coarse-grained”
bead-spring models as they are used in soft matter research in physics,
chemistry and molecular biology. It can be used to simulate systems such as
polymers, liquid crystals, colloids, ferrofluids and biological systems

ESPResSo is capable of doing classical Molecular dynamics simulations of many
types of systems in different statistical ensembles (NVE, NVT, NPT) and
non-equilibrium situations, using standard potentials such as the Lennard-Jones
or Morse potential. It contains many advanced simulation algorithms, which take
into account hydrodynamic (lattice Boltzmann) and electrostatic interactions
(P3M, ELC, MMMxD). Rigid bodies can be modelled by virtual site interactions,
and it can integrate rotationally non-invariant particles.

Further features include:

 * Statistical ensembles: NVE, NVT, NPT,  μVT
 * Nonbonded potentials: Lennard Jones, Buckingham, Morse, Generic tabulated
   potentials, etc.

 * Bonded potentials: harmonic spring, FENE, generic tabulated bonded
   potentials, etc.
 * Anisotropic interactions: e.g. Gay-Berne particles or other non-isotropic
   interactions
 * Advanced methods for electro- and magnetostatics: e.g. P3M, MMM1D, MMM2D,
   ELC, dipolar P3M, DLC, MEMD, MMM2DIC, ELCIC, ICC, LB-EK
 * Constraints: all or some of the  coordinates of particles can be fixed,
   various spatial constraints (walls, spheres, pores, …) can interact with the
   particles
 * Rigid bodies: arbitrary extended objects can be constructed from several
   particles
 * Dynamic bonding: when particles collide, new bonds can be generated between
   them to study agglomeration 
 * Hydrodynamics: Lattice-Boltzmann fluid (optionally on a GPGPU), DPD
   (Dissipative Particle Dynamics)

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
