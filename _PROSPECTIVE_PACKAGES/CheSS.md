---
Name: CheSS
Homepage: https://launchpad.net/chess
Reference:
  - URL: https://arxiv.org/abs/1704.00512
License: LGPL-3+
---

CheSS (standing for Chebyshev Sparse Solvers) is a package providing various
high level operations based on Chebyshev expansions for sparse matrices. Among
other functionalities, it provides an implementation of the Fermi Operator
Expansion (FOE) method to calculate the density matrix in an electronic
structure calculation and an efficient way of calculating the power of a sparse
matrix, i.e. A^x.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
