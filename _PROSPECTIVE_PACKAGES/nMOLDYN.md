---
Name: nMOLDYN
Package: python-nmoldyn
Version: 3.0.11
Released: 2017
Homepage: http://dirac.cnrs-orleans.fr/nMOLDYN.html
Repository: https://bitbucket.org/khinsen/nmoldyn3
Reference:
  - URL: http://dx.doi.org/10.1002/jcc.10243
License: CeCILL Free Software License
ITP: 642586
Vcs-Browser: https://salsa.debian.org/science-team/nmoldyn
Vcs-Git: https://salsa.debian.org/science-team/nmoldyn.git
---

nMOLDYN is an interactive analysis program for Molecular Dynamics simulations.
It is especially designed for the computation and decomposition of neutron
scattering spectra, but also computes other quantities.

Quantities related to neutron scattering:

 * coherent intermediate scattering function
 * memory function for coherent scattering function
 * dynamic structure factor
 * incoherent intermediate scattering function
 * elastic incoherent structure factor 

Other quantities:

 * mean-square displacement
 * velocity autocorrelation function
 * memory function for velocity autocorrelation function
 * density of states
 * angular velocity autocorrelation function (and its spectrum)
 * reorientational correlation function 

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
