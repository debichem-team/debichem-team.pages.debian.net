---
Name: Molsketch
Homepage: http://molsketch.sf.net
License: GPL-2+
---

2D molecular editing tool with its goal to help draw molecules quick and
easily. Creation can be exported afterwards in high quality in a number of
vector and bitmap formats.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
