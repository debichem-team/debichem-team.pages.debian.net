---
# vim: set tw=79 ts=2 sw=2 ai si et:
Name: MOLDY
Homepage: http://code.google.com/p/moldy/
License: GPL-2
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S001046551100261X
  - URL: https://arxiv.org/abs/1107.2619
Description: parallelised OpenMP short-ranged molecular dynamics program
---

MOLDY is a parallelised OpenMP short-ranged molecular dynamics program, first
written at Harwell Laboratory in the 1980s. The program is written in a
modular fashion to allow for easy user modification, in particular the
implementation of new interatomic potentials. Using Link Cells and Neighbour
Lists, the code fully exploits the short range of the potentials, and the
slow diffusion expected for solid systems.
