---
Name: Qbox
Homepage: http://eslab.ucdavis.edu/software/qbox/index.htm
Reference:
  - URL: http://eslab.ucdavis.edu/software/qbox/ibmr-52-01-Gygi.pdf
  - URL: http://dl.acm.org/citation.cfm?id=1188502
License: GPL
---

Qbox is a C++/MPI scalable parallel implementation of first-principles
molecular dynamics (FPMD) based on the plane-wave, pseudopotential formalism.
Qbox is designed for operation on large parallel computers.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
