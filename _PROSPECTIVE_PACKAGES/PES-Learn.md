---
Name: PES-Learn
Package: 
Version: 
Contact: 
Homepage: 
Repository: https://github.com/CCQC/PES-Learn
License: BSD-3-Clause
Lang: Python
Description: 
ITP: 
Vcs-Browser: 
Reference: 
  - URL: https://pubs.acs.org/doi/10.1021/acs.jctc.9b00312
---

PES-Learn is a Python library designed to fit system-specific Born-Oppenheimer
potential energy surfaces using modern machine learning models. PES-Learn
assists in generating datasets, and features Gaussian process and neural
network model optimization routines. The goal is to provide high-performance
models for a given dataset without requiring user expertise in machine
learning.

PES-Learn supports input file generation and output file parsing for arbitrary
electronic structure theory packages such as Psi4, Molpro, Gaussian, NWChem,
etc.

Data is generated with user-defined internal coordinate displacements with
support for:

 *  Redundant geometry removal
 *  Configuration space filtering

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
