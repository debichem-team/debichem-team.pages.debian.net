---
Name: SCHAKAL
Homepage: http://www.krist.uni-freiburg.de/de/content/download.html
License: public-domain
---

FORTRAN program for the graphical representation of molecular and
solid-state structure models.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
