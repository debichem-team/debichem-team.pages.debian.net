---
Name: PYXAID
Homepage: https://www.acsu.buffalo.edu/~alexeyak/pyxaid/index.html
Reference:
  - URL: http://pubs.acs.org/doi/abs/10.1021/ct400641n
License: GPL-3
---

The PYXAID program is developed for non-adiabatic molecular dynamics
simulations in condensed matter systems. By applying the classical path
approximation to the fewest switches surface hopping approach, it provides
an efficient computational tool that can be applied to study photoinduced
dynamics at the ab initio level in systems composed of hundreds of atoms and
involving thousands of electronic states.

PYXAID can be interfaced with Quantum ESPRESSO.

Capabilities include:
 * Basic non-adiabatic molecular dynamics: FSSH-CPA
 * Decoherence correction via DISH
 * Multi-electron basis functions
 * Direct photoexcitation via light-matter interaction
 * Advanced integration schemes for solving TD-SE
 * Numerous pre- and post-processing utilities

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
