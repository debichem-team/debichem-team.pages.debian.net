---
Name: QMCPACK
Homepage: http://qmcpack.org/
Repository: https://github.com/QMCPACK/qmcpack/
License: BSD
Comment: Other open-source QMC codes are QWalk and QMcBeaver
---

QMCPACK is a Quantum-Monte-Carlo code written in C++. It was developed at the
University of Illinois and is designed for high-performance computers. It
implements advanced QMC algorithms and is parallelized with both MPI and
OpenMP. Generic programming enabled by templates in C++ is extensively utilized
to achieve high efficiency on HPC systems.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
