---
Name: DP4
Repository: https://github.com/KristapsE/PyDP4
Reference:
  - URL: http://pubs.rsc.org/en/Content/ArticleLanding/2016/OB/C6OB00015K
License: MIT
Comment: requires TINKER, which is non-free
---

The DP4 parameter, which provides a confidence level for NMR assignment, has
been widely used to help assign the structures of many stereochemically-rich
molecules. 

TINKER  molecular  mechanics package and NWChem ab initio software have been
integrated in a fully open-source DP4 workflow.

The new open-source workflow incorporates a method for the automatic generation
of diastereomers using InChI strings and has been tested on a range of new
structures. This improved workflow permits the rapid and convenient
computational elucidation of structure and relative stereochemistry.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
