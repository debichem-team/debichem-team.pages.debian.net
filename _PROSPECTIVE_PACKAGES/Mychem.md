---
Name: Mychem
Package: mychem
Homepage: http://mychem.sourceforge.net
License: GPL
---

Mychem is a chemoinformatics extension for MySQL released under the GNU GPL
license. This extension provides a set of functions that permits to handle
chemical data within the MySQL database. These functions permit you to search,
analyze and convert chemical data.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
