---
Name: PCMSolver
Homepage: https://github.com/PCMSolver/pcmsolver
Repository: https://pcmsolver.readthedocs.io/en/stable/
License: LGPL-3
---

PCMSolver is an API for the polarizable continuum model (PCM), a continuum
solvation model. PCMSolver can be interfaced to a quantum chemistry host
program to provide PCM functionality. In the current release the basic features
of the PCM are made available:

 * GePol cavity generator;
 * IEFPCM and CPCM solvers for isotropic uniform dielectrics;
 * flexible Green's function definition for isotropic uniform dielectrics.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
