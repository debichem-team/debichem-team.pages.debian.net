---
Name: PHAISTOS
Homepage: http://www.phaistos.org
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.23292/abstract
License: GPL-3
---

PHAISTOS is a Markov chain Monte Carlo framework for protein structure
simulations. It contains a variety of both established and novel moves types,
and provides support for several force-fields from the literature. In addition,
an interface to the Muninn generalized ensemble package makes it possible to
easily conduct multi-histogram based simulations, avoiding the convergence
problems often associated with Metropolis-Hastings based sampling.

A unique feature of PHAISTOS is the use of probabilistic models to capture
essential structural properties in proteins. These models are available both as
proposal distributions (moves), and for likelihood evaluations (energies). This
increases the flexibility when settings up a simulation, by allowing the user
to choose how to incorporate the bias provided by these models in the
simulation. For instance, similar to the use of fragment or rotamer libraries,
using probabilistic models for sampling of backbone angles and sidechain angles
corresponds to having an implicit energy term present in the simulation. Unlike
fragment and rotamer libraries, however, when using probabilistic models, this
term can be evaluated and compensated for if necessary. PHAISTOS currently
incorporates models for the CA-only representation of protein backbones
(FB5HMM), full-atom backbones (TORUSDBN), full-atom sidechains (BASILISK), and
single-mass sidechains (COMPAS). 

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
