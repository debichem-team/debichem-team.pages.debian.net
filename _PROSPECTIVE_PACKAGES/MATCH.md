---
Name: MATCH
Package: 
Version: 
Contact: 
Homepage: http://brooks.chem.lsa.umich.edu/index.php?page=match&subdir=articles/resources/software
Repository: 
License: Perl
Lang: 
Description: 
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.21963/abstract
ITP: 
---

A toolset of program libraries collectively titled multipurpose atom-typer for
CHARMM (MATCH) for the automated assignment of atom types and force field
parameters for molecular mechanics simulation of organic molecules. A general
chemical pattern-matching engine achieves assignment of atom types, charges and
force field parameters through comparison against a customizable list of
chemical fragments.

(download behind dumb registration site)

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
