---
Name: LiBELa
Reference:
  - URL: http://link.springer.com/article/10.1007%2Fs10822-015-9856-1
License: GPL
Lang: C++
---

LiBELa is a C++ Object-Oriented framework for ligand binding energy evaluation.
It allows the user to (i) dock a ligand into a macromolecular receptor; (ii)
perform simulated annealing simulations of the ligand dynamics inside the
receptor binding site and (iii) perform Monte Carlo (MC) dynamic of the ligand
in the binding site.

The docking capabilities are probably the most important among the McLiBELa
functions. It permits the user to provide any MOL2 file for a ligand (from ZINC
website, for example) and search for the best poses of the provided ligand
maximizing the overlay of the ligand with a reference ligand, provided to the
program. LiBELa also performs a search for the conformers with lower energy
prior to ligand docking.

The docking capability of LiBELa can be described in the following steps:

 * Conformer generations. The user chooses one of the two conformer generators
   provided by openbabel API (weighted rotor search or genetic algorithm) and
   the number of conformers.  
 * Each conformer is superposed in the reference ligand looking for optimal
   overlay. Gaussian descriptors of molecular shape and charge are used to
   generate an optimal overlay.
 * Each conformer docked in the active site is scored using one of the four
   scoring functions implemented in LiBELa (see below). The top n conformers
   ranked using binding energy as criteria are then used in a simplex
   minimization in Cartesian space (rigid ligand).
 * The best conformer is written in a MOL2 file. 

LiBELa uses NLOPT library for optimization. Among the optimizers provided by
NLOPT, the user can choose among LBFGS, Simplex, Augmented Lagrangian, MMA,
among other algorithms for overlay optimization and for energy minimization
independently.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
