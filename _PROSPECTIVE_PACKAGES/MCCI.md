---
Name: MCCI
Repository: https://github.com/MCCI/mcci
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S0010465500001193
  - URL: http://scitation.aip.org/content/aip/journal/jcp/103/5/10.1063/1.469756
License: none
Comment: No license so far (see https://github.com/MCCI/mcci/issues/1).
---

Monte Carlo Configuration Interaction (MCCI) is a computer program that
calculates electronic structure of molecular systems using a method based on
Configuration Interaction (CI). In conventional CI, all configurations (usually
spin-projected Slater Determinants, or Configuration State Functions (CSFs)) up
to a given excitation level are included in the wave function. While this
produces a highly accurate wave function, the number of CSFs to include (and
thus also the dimensions of the matrix to be diagonalized) increases
combinatorially with the truncation level. In addition, the fixed truncation
level of conventional CI causes the method to be non-size-consistent.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
