---
Name: PDB Validation Suite
Homepage: http://sw-tools.pdb.org/apps/VAL/index.html
License: UNKNOWN
---

The PDB Validation Suite is a set of tools used by the PDB for processing and
checking structure data.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
