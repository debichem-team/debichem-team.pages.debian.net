---
Name: Quantum Package
Repository: https://github.com/QuantumPackage/qp2
License: AGPL-3.0
Reference:
  - URL: https://pubs.acs.org/doi/full/10.1021/acs.jctc.9b00176
---

The Quantum Package is an open-source programming environment for quantum
chemistry. It has been built from the developper point of view in order to help
the design of new quantum chemistry methods, especially for wave function
theory (WFT).

From the user point of view, the Quantum Package proposes a stand-alone path to
use optimized selected configuration interaction sCI based on the CIPSI
algorithm that can efficiently reach near-full configuration interaction FCI
quality for relatively large systems.

The Quantum Package is not a general purpose quantum chemistry program. First
of all, it is a library to develop new theories and algorithms in quantum
chemistry. Therefore, beside the use of the programs of the core modules, the
users of the Quantum Package should develop their own programs.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
