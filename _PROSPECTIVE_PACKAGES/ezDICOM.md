---
Name: ezDICOM
Homepage: http://www.cabiatl.com/mricro/ezdicom/
License: BSD-2-clause
---

This software is designed to display most medical images: MRI, CT, X-ray, and
ultrasound. All versions of ezDICOM can automatically detect the format of a
medical image and display it on the screen.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
