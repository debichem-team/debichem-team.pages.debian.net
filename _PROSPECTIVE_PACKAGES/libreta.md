---
Name: libreta
Homepage: http://www.zhjun-sci.com/software-libreta-download.php
Reference:
  - URL: http://pubs.acs.org/doi/abs/10.1021/acs.jctc.7b00788
License: BSD 3-Clause
Lang: C++
---

Libreta is a C++ library for the evaluation of molecular integrals over both
segmented and general contracted Gaussian type orbitals (GTOs). It is written
in a black-box way that one can use it without knowing anything inside.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
