---
Name: MDANSE
Homepage: https://mdanse.org/
Repository: https://github.com/mdanseproject/MDANSE
Reference:
  - URL: http://pubs.acs.org/doi/abs/10.1021/acs.jcim.6b00571
License: GPL-3
---

MDANSE (Molecular Dynamics Analysis for Neutron Scattering Experiments) is a
python application designed for computing properties that can be directly
compared with neutron scattering experiments such as the coherent and
incoherent intermediate scattering functions and their Fourier transforms, the
elastic incoherent structure factor, the static coherent structure factor or
the radial distribution function. Moreover, it can also compute quantities such
as the mean-square displacement, the velocity autocorrelation function as well
as its Fourier Transform (the so-called vibrational density of states)
enlarging the scope of the program to a broader range of physico-chemical
properties.

Most of MDANSE calculations can be applied to the whole system or to arbitrary
subsets that can be defined in the graphical interface while less common
selections can be specified via the command-line interface.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
