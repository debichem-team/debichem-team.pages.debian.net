---
Name: PySCF
Homepage: http://sunqm.net/pyscf/index.html
Repository: https://github.com/sunqm/pyscf
Reference:
  - URL: https://arxiv.org/abs/1701.08223
License: BSD 2-clause
---

Pyscf is a quantum chemistry package written in python. The package aims to
provide a simple, light-weight and efficient platform for quantum chemistry
code developing and calculation.

Common quantum chemistry methods:

 * Hartree-Fock
 * DFT
 * CASCI and CASSCF
 * Full CI
 * MP2
 * SC-NEVPT2
 * CCSD and CCSD(T)
 * CCSD lambda
 * EOM-CCSD
 * Density fitting
 * relativistic correction
 * General integral transformation
 * Gradients
 * NMR
 * TDDFT

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
