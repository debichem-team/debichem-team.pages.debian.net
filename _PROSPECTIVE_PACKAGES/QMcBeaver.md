---
Name: QMcBeaver
Homepage: http://qmcbeaver.sourceforge.net/
License: GPL
---

QMcBeaver is a finite, all electron variational and diffusion quantum Monte
Carlo program. 

Several features distinguish QMcBeaver from other QMC programs:

 * The code runs on massively parallel architectures, and has a highly efficient
 decorrelation algorithm to properly decorrelate the data from different
 processors.
 * The code uses a Manager-Worker based parallelization scheme which allows the
 efficient use of heterogeneous clusters.
 * The code is written in modern object-oriented C++, and is clean, and easily
 understood and modified.

QMcBeaver can use trial wavefunctions from GAMESS and Jaguar.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
