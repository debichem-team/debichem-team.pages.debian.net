---
Name: Dalton LSDalton
Homepage: http://daltonprogram.org
Repository: https://gitlab.com/dalton/
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/wcms.1172/abstract
License: LGPL-2.1
---

The Dalton suite consists of two separate executables, Dalton and LSDalton.
The Dalton code is a powerful tool for a wide range of molecular properties at
different levels of theory, whereas LSDalton is a linear-scaling HF and DFT
code suitable for large molecular systems, now also with some CCSD capabilites.

For a full list of features, see http://daltonprogram.org/features/

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
