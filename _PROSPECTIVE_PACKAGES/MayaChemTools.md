---
Name: MayaChemTools
Homepage: http://www.mayachemtools.org
License: LGPL
---

MayaChemTools is a growing collection of Perl scripts, modules and classes to
support day-to-day computational discovery needs.

The current release of MayaChemTools provides command line scripts for the
following tasks:

 * Manipulation of SD, CSV/TSV, Sequence/Alignments and PDB files
 * Analysis of data in SD, CSV/TSV and Sequence/Alignments files
 * Information about data in SD, CSV/TSV, Sequence/Alignments, PDB and
   fingerprints files
 * Exporting data from Oracle and MySQL tables into text files
 * Properties of periodic table elements, amino acids and nucleic acids
 * Elemental analysis
 * Generation of fingerprints corresponding to atom neighborhoods, atom types,
   E-state indicies, extended connectivity, MACCS keys, path lengths,
   topological atom pairs, topological atom triplets, topological atom
   torsions, topological pharmacophore atom pairs and topological
   pharmacophore atom triplets
 * Generation of fingerprints with atom types corresponding to atomic
   invariants, DREIDING, E-state, functional class, MMFF94, SLogP, SYBYL, TPSA
   and UFF
 * Calculation of similarity matrices using a variety of similarity and
   distance coefficients
 * Calculation of physicochemical properties including rotatable bonds, van
   der Waals molecular volume, hydrogen bond donors and acceptors, logP
   (SLogP), molar refractivity (SMR), topological polar surface area (TPSA),
   molecular complexity and so on
 * Similarity searching using fingerprints

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
