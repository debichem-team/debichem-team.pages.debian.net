---
Name: v2rdm_casscf
Repository: https://github.com/edeprince3/v2rdm_casscf
Reference:
  - URL: http://pubs.acs.org/doi/abs/10.1021/acs.jctc.6b00190
License: GPL-2+
---

A variational 2-RDM-driven CASSCF plugin to Psi4. This plugin to Psi4
performs variational two-electron reduced-density-matrix (2-RDM)-driven
complete active space self consistent field (CASSCF) computations. In
principle, because variational 2-RDM (v-2RDM) methods scale only polynomially
with system size, v-2RDM-driven CASSCF computations can be performed using
active spaces that are larger than can be used within conventional
configuration-interaction-driven CASSCF methods.

