---
Name: MDAnalysis
Package: python-mdanalysis
Version: 
Contact: 
Homepage: http://www.mdanalysis.org
Repository: https://github.com/MDAnalysis/mdanalysis
License: GPL-2, BSD
Lang: 
Description: 
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.21787/abstract
ITP: 
Vcs-Browser: https://salsa.debian.org/debichem-team/python-mdanalysis
Vcs-Git: https://salsa.debian.org/debichem-team/python-mdanalysis.git
---

MDAnalysis is an object-oriented python toolkit to analyze molecular dynamics
trajectories generated in many popular formats including CHARMM, Gromacs, NAMD,
LAMMPS and Amber.

It allows one to read molecular dynamics trajectories and access the atomic
coordinates through numpy arrays. This provides a flexible and relatively fast
framework for complex analysis tasks. In addition, CHARMM-style atom selection
commands are implemented. Trajectories can also be manipulated (for instance,
fit to a reference structure) and written out.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
