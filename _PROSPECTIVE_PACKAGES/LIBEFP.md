---
Name: LIBEFP
Repository: https://github.com/ilyak/libefp
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.23375/abstract
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.23772/abstract
License: BSD
---

The Effective Fragment Potential (EFP) method allows one to describe large
molecular systems by replacing chemically inert part of a system by a set of
Effective Fragments while performing regular ab initio calculation on the
chemically active part. The LIBEFP library is a full implementation of the EFP
method. It allows users to easily incorporate EFP support into their favourite
quantum chemistry package. LIBEFP is used by many major quantum chemistry
packages, such as Q-Chem, PSI4, NWChem, GAMESS, Molcas, and others.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
