---
Name: AmberTools
Package: 
Version: 
Contact: 
Homepage: http://ambermd.org/#AmberTools
License: GPL-3
Lang: 
Description: 
Reference:
  - URL: 
ITP: 
---

AmberTools consists of several independently developed packages that work well
by themselves, and with Amber itself. The suite can also be used to carry out
complete (non-periodic) molecular dynamics simulations (using NAB), with
generalized Born solvent models.

<!-- # vim: set tw=79 ts=2 sw=2 ai si et: -->
