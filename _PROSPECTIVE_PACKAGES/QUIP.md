---
Name: QUIP
Repository: https://github.com/libAtoms/QUIP
License: GPL-2
---

The libAtoms package is a software library written in Fortran 95+ for the
purposes of carrying out molecular dynamics simulations. The QUIP package,
built on top of libAtoms, implements a wide variety of interatomic potentials
and tight binding quantum mechanics, and is also able to call external
packages. Various hybrid combinations are also supported in the style of QM/MM.

We try to strike a compromise between readability of code and efficiency, and
think of QUIP/libAtoms as a "developer's code": nice when you want to try new
ideas quickly, but not competitive in efficiency with other major md codes such
as LAMMPS, Gromacs etc. We use several extensions to the Fortran 95 standard in
order to make the coding style more object oriented. Several compilers support
all the necessary extensions in their recent versions, e.g. GNU v4.4 and later.
Support in the Intel compiler suite is there in principle, but not every recent
version has correct implementation, although we have not encountered many
problems past version 11. Get in touch for a list of versions known to compile
correctly if you encounter difficulties. 

The following interatomic potentials are presently coded or linked in QUIP:

 *  BKS (van Beest, Kremer and van Santen) (silica)
 *  EAM (fcc metals)
 *  Fanourgakis-Xantheas (water)
 *  Finnis-Sinclair (bcc metals)
 *  Flikkema-Bromley
 *  GAP (Gaussian Approximation Potentials: general many-body)
 *  Guggenheim-!McGlashan
 *  Brenner (carbon)
 *  OpenKIM (general interface)
 *  Lennard-Jones
 *  Morse
 *  Partridge-Schwenke (water monomer)
 *  Stillinger-Weber (carbon, silicon, germanium)
 *  SiMEAM (silicon)
 *  Sutton-Chen
 *  Tangney-Scandolo (silica, titania etc)
 *  Tersoff (silicon, carbon) 

The following tight-binding functional forms and parametrisations are implemented:

 *  Bowler
 *  DFTB
 *  GSP
 *  NRL-TB 

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
