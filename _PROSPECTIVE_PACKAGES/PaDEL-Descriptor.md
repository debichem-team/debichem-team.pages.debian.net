---
Name: PaDEL-Descriptor
Package: 
Version: 
Contact: 
Homepage: http://padel.nus.edu.sg/software/padeldescriptor/
Repository: 
License: AGPLv3+, Apachev2, LGPLv2+, GPLv2/CDDL
Lang: 
Description: 
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.21707/abstract
ITP: 
---

A software to calculate molecular descriptors and fingerprints. The software
currently calculates 856 descriptors (722 1D, 2D descriptors and 134 3D
descriptors) and 10 types of fingerprints. The descriptors and fingerprints are
calculated using The Chemistry Development Kit (CDK) with some additional
descriptors and fingerprints. These additions include atom type
electrotopological state descriptors, extended topochemical atom (ETA)
descriptors, McGowan volume, molecular linear free energy relation descriptors,
ring counts, count of chemical substructures identified by Laggner, and binary
fingerprints and count of chemical substructures identified by Klekota and
Roth.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
