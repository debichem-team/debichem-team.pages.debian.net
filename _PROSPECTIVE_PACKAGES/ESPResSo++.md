---
Name: ESPResSo++
Homepage: http://www.espresso-pp.de
Repository: https://github.com/espressopp/espressopp
Reference:
  - URL: http://www.sciencedirect.com/science/article/pii/S0010465512004006
License: GPL-3
---

ESPResSo++ is an extensible, flexible and parallel simulation software for the
scientific simulation and analysis of coarse-grained atomistic or bead-spring
models as they are used in soft matter research. 

Features include:

 * Molecular Dynamics: 
  - Langevin thermostat and barostat
  - Berendsen thermostat and barostat
  - Rattle and Settle algorithms
  - Angular and dihedral interactions
  - Bonded and non-bonded interactions (FENE, Lennard-Jones, etc.)
  - Tabulated interactions
 * Adaptive Resolution Simulations (AdResS)
  - F-AdResS
  - H-AdResS
 * Lattice Boltzmann
  - Coupling to MD via viscous drag
  - Robust time-scale separation between MD and LB
  - External forces

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
