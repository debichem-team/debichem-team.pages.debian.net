---
Name: LBNL-AMO-MCTDHF
Homepage: https://commons.lbl.gov/display/csd/LBNL-AMO-MCTDHF
Reference:
  - URL: http://journals.aps.org/pra/abstract/10.1103/PhysRevA.83.063416
License: Apache License
Comment: though it includes a file expokit.f with dubious origin
---

LBNL-AMO-MCTDHF is a suite of codes for Multiconfiguration Time-Dependent
Hartree-Fock applied to ultrafast laser dynamics of atoms and molecules. It
calculates nonadiabatic electronic and nuclear wave functions for the
nonrelativistic Schrödinger equation. Currently it uses the dipole
approximation in length and velocity gauge and has options for a variety of
laser pulses. It contains analysis and output routines and auxiliary scripts
for calculating absorption and stimulated emission, populations, total and
partial photoionization, wave mixing, and other capabilities. It supports

 * Electronic wave functions for atoms (chmctdhf_atom)
 * Vibronic wave functions for diatoms using prolate spheroidal coordinates
  (chmctdhf_diatom)
 * Electronic wave functions for polyatomics with fixed nuclei (chmctdhf_sinc)

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
