---
Name: ORBKIT
Repository: https://github.com/orbkit/orbkit/
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.24358/abstract
License: LGPL-3
---

ORBKIT is a toolbox for postprocessing electronic structure calculations based
on a highly modular and portable Python architecture. The program allows
computing a multitude of electronic properties of molecular systems on
arbitrary spatial grids from the basis set representation of its electronic
wavefunction, as well as several grid-independent properties. The required data
can be extracted directly from the standard output of a large number of quantum
chemistry programs. ORBKIT can be used as a standalone program to determine
standard quantities, for example, the electron density, molecular orbitals, and
derivatives thereof

The required data can be extracted from MOLPRO (Molden File Format), TURBOMOLE
(AOMix file format), GAMESS-US, PROAIMS/AIMPAC (wfn/wfx file format), and
Gaussian (.log File and Formatted Checkpoint File) output files. Futhermore, an
interface to cclib, a parser for quantum chemical logfiles, is provided.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
