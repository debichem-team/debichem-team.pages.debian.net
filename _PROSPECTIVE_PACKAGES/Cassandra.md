---
Name: Cassandra
Homepage: https://cassandra.nd.edu/
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.24807/abstract
License: GPL-2
Registration: https://cassandra.nd.edu/index.php/component/users/?view=registration&Itemid=115
---

Cassandra is an open source Monte Carlo package capable of simulating any
number of molecules composed of rings, chains, or both. It can be used to
simulate small organic molecules, oligomers, and ionic liquids. It handles a
standard "Class I"-type force field having fixed bond lengths, harmonic bond
angles and improper angles, a CHARMM or OPLS-style dihedral potential, a
Lennard-Jones 12-6 potential or Mie potential and fixed partial charges.
Cassandra can simulated the following ensembles: canonical (NVT),
isothermal-isobaric (NPT), grand (muVT), osmotic (muPT), and Gibbs (NVT and NPT
versions).

Cassandra uses OpenMP parallelization and comes with a number of scripts,
utilities and examples to help with simulation setup. You can download it for
free from this site. You will be asked for some minimal information before
downloading the code so that we can let you know when new versions are released

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
