---
Name: Coot
Package: coot
Version: 0.8.9.1
Released: 2018
Contact: Paul Emsley <paul.emsley@bioch.ox.ac.uk>
Homepage: https://www2.mrc-lmb.cam.ac.uk/Personal/pemsley/coot/
License: GPL-3+
Lang: C++
Description: model building program for macromolecular crystallography
Reference:
  - URL: https://journals.iucr.org/d/issues/2010/04/00/ba5144/
ITP: 897673
Vcs-Browser: https://salsa.debian.org/science-team/coot
Vcs-Git: https://salsa.debian.org/science-team/coot.git
---

Coot is a molecular-graphics application for model building and validation of
biological macromolecules. The program displays electron-density maps and
atomic models and allows model manipulations such as idealization, real-space
refinement, manual rotation/translation, rigid-body fitting, ligand search,
solvation, mutations, rotamers and Ramachandran idealization. Furthermore,
tools are provided for model validation as well as interfaces to external
programs for refinement, validation and graphics.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
