---
Name: Janus
Package: 
Version: 
Contact: 
Homepage: 
Repository: https://github.com/CCQC/janus
License: BSD-3-Clause
Lang: 
Description: 
ITP: 
Vcs-Browser: 
Reference: 
  - URL: https://pubs.acs.org/doi/10.1021/acs.jctc.9b00182
---

Janus is a open-source python library for the implementation and application of
adaptive QM/MM methods.

The following adaptive methods are currently available in janus.

 * Hot-Spot
 * Oniom-XS
 * Permuted Adaptive Partitioning
 * Sorted Adaptive Partitioning
 * Difference-based Adaptive Partitioning

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
