---
Name: MSL
Homepage: http://msl-libraries.org/index.php/Main_Page
Reference:
  - URL: http://onlinelibrary.wiley.com/doi/10.1002/jcc.22968/abstract
License: LGPL
---

MSL is a library for analysis, manipulation, modeling and design of
macromolecules. 

Features include:

 * Support for reading and writing PDB and CRD files.
 * The ability of storing and switching between multiple atom coordinates, for
   conformational changes and rotameric representation of side chain
   conformational freedom.
 * For protein design, the ability to build and store multiple residue
   identities (i.e. LEU, ILE, ALA) at a position and switch between them.
 * Support for rotamer libraries.
 * Transformations such as translations, rotations, and alignments.
 * The CHARMM force field and other energy funtions.
 * Support for CHARMM topology and parameter files.
 * Structure building from scratch (using internal coordinates)
 * Search algorithms such as Monte Carlo, Dead End Elimination, and Self
   Consistent Mean Field Monte Carlo.
 * Local backbone sampling.
 * Crystal lattice generation.
 * A PyMOL Python Interface for calling MSL code from within PyMOL.
 * A R Interface for calling arbitrary R algorithms or plotting routines from
   within MSL

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
