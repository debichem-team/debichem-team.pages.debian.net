---
Name: Wannier90
Package: wannier90
Version: 3.0.0
Released: 2019
Contact: Arash Mostofi <a.mostofi@imperial.ac.uk>
Homepage: http://www.wannier.org/
Repository: https://github.com/wannier-developers/wannier90
License: GPL
Lang: Fortran 90
Description: Maximally Localized Wannier Functions
ITP: 578829
Reference: 
  - URL: https://arxiv.org/abs/1907.09788
Other-References: http://www.wannier.org/papers/
Vcs-Browser: https://salsa.debian.org/science-team/wannier90
Vcs-Git: https://salsa.debian.org/science-team/wannier90.git
---

Wannier90 is an electronic-structure software computing maximally-localized
Wannier functions (MLWF). It works on top of other electronic-structure
software, such as Abinit, FLEUR, and PwSCF.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
