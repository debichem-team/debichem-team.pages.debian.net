---
Name: ccwatcher
Homepage: http://ccwatcher.sourceforge.net/
License: GPL
---

ccwatcher monitors the progress of computational chemistry calculations during
their runtime. It parses important output and plots SCF energies. ccwatcher is
highly platform-independent thanks to Python and the Qt Toolkit.

It Supports most computational chemistry programs via cclib and includes a GUI
and command-line interface.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
