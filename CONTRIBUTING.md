---
layout: page
title: Contribute
description: >
  The debichem team uses Git to package chemical software.
permalink: /git/
---

The debichem team uses GIT to store the packaging files. There are two
different layouts, that are used.

1. Both the Upstream sources and the packaging files are stored in Git. Using
`pristine-tar` it is possible to recreate identical tarballs from the Git
source.

1. Only the packaging files (only the debian/ directory) is stored in Git. This
is called the _overlay_ layout. The tarball can be retrieved by `origtargz` or
a similar tools.

Both layouts can be easily handled by `git-buildpackage`. They are just a bit
different in the configuration and the options to use. For the moment we don't
enforce one or the other, but prefer the **latter**!

## Common Git packaging practices

Please make sure, that the name and email address and the signing key, if
existent, are set correctly for Git (either locally, as shown below, or
globally):

```
git config --local user.name "Your name"
git config --local user.email "yourmail@example.org"
git config --local user.signingkey "yourkey"
```
{: title="Configuring .git/config"}

### Common git-buildpackage configurations

It is not mandatory to use `git-buildpackage`, but the tool is easy to use and
also used for [CI/CD](#cicd){: title="Continous Integration/Continuous
Delivery/Continous Deployment"}. There is [detailed
documentation](https://honk.sigxcpu.org/projects/git-buildpackage/manual-html/).
The following sections will ionly cover the basics and get you up and running
in no time.

The configuration can be done in several files:
_/etc/git-buildpackage/gbp.conf_ (system-wide), _~/.gbp.conf_ (per user),
_debian/gbp.conf_ (per repository, distributable) and _.git/gbp.conf_ (per
repository, not distributed). If you need a skeleton, just copy
_/etc/git-buildpackage/gbp.conf_ to any of the other locations. The options are
explained in gbp.conf(**5**) too.

iSome important options, that you maybe want to set or check in _~/.gbp.conf_:

```
[DEFAULT]
builder = ...
pristine-tar = true

[buildpackage]
sign-tags = true
keyid = <key-id>
export-dir = /tmp/build-area/
notify = off

[import-orig]
filter-pristine-tar = true
```
{: title="~/.gbp.conf"}

### Overlay layout or debian/-only

For this create a new project. If you are not yet a team member, you can use
your own personal space to do this. Otherwise [creating a new project in the
debichem group](https://salsa.debian.org/projects/new?namespace_id=2227) space
is fine too.

Add your packaging files - the _debian_ directory - and the file
_debian/gbp.conf_ with this content:

```
[DEFAULT]
pristine-tar = false
debian-branch = master
verbose = true

[buildpackage]
overlay = true
```
{: title="debian/gbp.conf"}

Because the Git repository will not store the upstream sources in this layout,
git-buildpackage needs to be told, where to find the tarball. This can be done
by setting the tarball variable in _~/.gbp.conf_ to the place, where you store
the tarballs. The default value is _../tarballs/_. My file for example looks
like this:

```
[buildpackage]
tarball-dir = ~/.local/src/tarballs/
```
{: title="~/.gbp.conf"}

Adjust the path to your tarball location. The source tarball can be easily
obtained by running `origtargz -dt`. The tool will try different package
sources and run e.g. `apt-get source` or `uscan -dd --download-current-version`
depending of where the tarball is available. Then the tarball should be moved
to the desired location.  In the next step the package can be built:

```
$ gbp buildpackage
```

### Pristine-tar layout with master, upstream and pristine-tar branch



### DEP-14

TODO

## CI/CD

Packages can be built using gitlabs Continous Integration (CI). After every
push, a pipeline of scripts is run to build, test, and validate the code
changes. It is even possible to check the resulting package with `lintian`,
`autopkgtest`, `reprotest` and more. The configuration is done by a file called
_debian/salsa-ci.yml_.

Because the default file name and location is a bit different from what we use
for Debian, the path in

**Project > Settings > CI/CD > General pipelines (Expand) > Custom CI config path**

must be set to _debian/salsa-ci.yml_. This can also be done using the Gitlab
API and your generated token:

```shell
curl -X PUT \
     -H "Content-Type: application/json" \
     -H "PRIVATE-TOKEN:__TOKEN__" \
     --data '{"ci_config_path":"debian/salsa-ci.yml"}' \
     https://salsa.debian.org/api/v4/projects/debichem-team%2F__PROJECT__
```

To not have every project write its own files and pipelines, the [Salsa CI
Team](https://salsa.debian.org/salsa-ci-team) has created the
[pipeline](https://salsa.debian.org/salsa-ci-team/pipeline) project. To enable
building the package, the _debian/salsa-ci.yml_ file only needs this content:

```yaml
include:
- https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/salsa-ci.yml
- https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/pipeline-jobs.yml
```
{: title="debian/salsa-ci.yml"}

Including _pipeline-jobs.yml_ enables all jobs inlcuding building the package
and testing the results. Usually it is not necessary to add anything else. But
if you want to build the package for a different suite than Sid, you can add
variables, which are described in the
[documentation](https://salsa.debian.org/salsa-ci-team/pipeline).

This file should not be exported into the Debian package source. Therefor add
this entry to _.gitattributes_

```text
debian/.gitattributes export-ignore
debian/salsa-ci.yml export-ignore
```
{: title=".gitattributes"}

## Hooks

There is currently just the
[tagpending](https://salsa.debian.org/salsa/salsa-webhook) webhook enabled for
all project repositories, which is triggered by push events. This hook can be
set either via API:

```shell
curl -X POST \
     -H "Content-Type: application/json" \
     -H "PRIVATE-TOKEN:__TOKEN__" \
     --data '{"url":"https://webhook.salsa.debian.org/tagpending/__PROJECT__"}' \
     https://salsa.debian.org/api/v4/projects/debichem-team%2F__PROJECT__/hooks
```

or by manually configuring a push event trigger URL at

**Project > Settings > Integration**

using `https://webhook.salsa.debian.org/tagpending/__PROJECT__` replacing
`__PROJECT__` with the actual [project
name](https://salsa.debian.org/debichem-team) on salsa.debian.org.

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
