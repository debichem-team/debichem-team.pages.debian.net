---
layout: page
title: Removals
description: >
  This file covers all removals from our subversion tree.
permalink: /removals/
---

This file covers all removals from our old subversion tree. You can still get
them from [our Git repository](https://salsa.debian.org/debichem-team/wnpp).

{% assign pkgs = site.REMOVED_PACKAGES | sort_natural: 'Name' %}
{% for pkg in pkgs %}

{{ pkg.content | markdownify }}

{% endfor %}

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
