---
layout: home
title: The debichem group
Description: >
  The debichem group is a team of package maintainers that share interest in
  chemical applications and their packaging for Debian and Ubuntu.
permalink: /
---

## We are ...

... a team of package maintainers that share interest in chemical applications
and their packaging for Debian and Ubuntu.

## Join us ...

... if you are interested in chemical suites and programs packaged for Debian
and Ubuntu. There is a [list of prospective packages][PPKG] which are deemed
to be interesting for the Debian users. If you are not experienced in
packaging you can also help by reporting issues, or sending patches for
reported issues.

For access to the Gitlab service an account on [salsa.debian.org][Salsa] is
necessary.  With a user account you can then [request access][Join].

[Salsa]: https://salsa.debian.org 
[Join]: https://salsa.debian.org/groups/debichem-team/-/group_members/request_access "Join the debichem team"
[PPKG]: ./prospectives/

## We use ...

1. [debichem-devel@lists.alioth.debian.org][ML] [(Archive)][MLA] for coordinating our development work and receiving bug reports from the BTS

1. [#debichem (IRC)][IRC] for real time communication

1. [debichem-team Git repositories][GIT] for packaging efforts based on [Git](./git/)

[ML]: mailto:debichem-devel@lists.alioth.debian.org
[MLA]: https://lists.alioth.debian.org/pipermail/debichem-devel/ "debichem-devel mailing list archive"
[IRC]: irc://irc.freenode.org/debichem
[GIT]: https://salsa.debian.org/debichem-team

*[IRC]: Internet Relay Chat
*[BTS]: Bug Tracking System

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
