---
layout: page
title: Prospective Packages
description: >
  The site holds a list of prospective packages for the Debichem project. Just because a
  project is listed here does not mean it needs to or will be packaged
  eventually.
permalink: /prospectives/all/
---

These are prospective packages for the Debichem project. Just because a
project is listed here does not mean it needs to be packaged eventually, just
that it was deemed interesting (i.e. is open-source and chemistry-related) at
one point.

{% assign pkgs = site.PROSPECTIVE_PACKAGES | sort_natural: 'Name' -%}

{%- for pkg in pkgs -%}

## {{ pkg.Name }} {% if pkg.ITP or pkg.Vcs-Browser -%}
(
{%- if pkg.ITP -%}
[ITP](https://bugs.debian.org/{{ pkg.ITP }})
{%- if pkg.Vcs-Browser -%}
,
{%- endif -%}
{%- endif -%}
{%- if pkg.Vcs-Browser -%}
[VCS]({{ pkg.Vcs-Browser }})
{%- endif -%}
)
{%- endif %}

```text
* Package name    : {{ pkg.Package }}
  Version         : {{ pkg.Version }}
  Upstream Author : {{ pkg.Contact }}
* URL             : {{ pkg.Homepage | default: pkg.Repository }}
* License         : {{ pkg.License }}
  Programming Lang: {{ pkg.Lang }}
  Description     : {{ pkg.Description }}
```

{{ pkg.content | markdownify }}

{% endfor %}

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
