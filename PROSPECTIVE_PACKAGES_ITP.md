---
layout: page
title: Prospective Packages with an ITP Bug
description: >
  The site holds a list of prospective packages for the Debichem project, for
  which an ITP bug report has already been filed.
permalink: /prospectives/itp/
---

These are prospective packages for the Debichem project, for which an ITP bug
report has already been filed. Just because a project is listed here does not
mean it needs to be packaged eventually, just that it was deemed interest in a
person, who wants to package it.

{% assign pkgs = site.PROSPECTIVE_PACKAGES | where_exp: "item","item.ITP" | sort_natural: 'Name' -%}

{%- for pkg in pkgs -%}

## [{{ pkg.Name }}]({{ pkg.Homepage | default: pkg.Repository }}) {% if pkg.ITP or pkg.Vcs-Browser -%}
(
{%- if pkg.ITP -%}
[ITP](https://bugs.debian.org/{{ pkg.ITP }})
{%- if pkg.Vcs-Browser -%}
,
{%- endif -%}
{%- endif -%}
{%- if pkg.Vcs-Browser -%}
[VCS]({{ pkg.Vcs-Browser }})
{%- endif -%}
)
{%- endif %}

```text
* Package name    : {{ pkg.Package }}
  Version         : {{ pkg.Version }}
  Upstream Author : {{ pkg.Contact }}
* URL             : {{ pkg.Homepage | default: pkg.Repository }}
* License         : {{ pkg.License }}
  Programming Lang: {{ pkg.Lang }}
  Description     : {{ pkg.Description }}
```

{{ pkg.content | strip_html | markdownify }}

{% endfor %}

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
